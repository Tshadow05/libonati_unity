﻿using UnityEngine;
using System.Collections;
using SimpleJSON;
using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;

public class Network_UDP_Sender : SocketConnection
{
	private UdpSender udp;
	private NetworkDebuggingTool debugTool;

	void Awake(){
		debugTool = gameObject.GetComponent<NetworkDebuggingTool> ();
	}

	public override void initializePort ()
	{
		base.initializePort ();
		udp = UdpSender.NewUnicast (ip, port);
	}

	public void sendMessage(byte[] data){
#if UNITY_EDITOR
		//Debug.Log ("Sending Message: " + System.Text.Encoding.ASCII.GetString (data));
#endif
		if (debugTool != null && debugTool.debugging) {
			debugTool.sendNetworkMessage ();
		}
		udp.Send (data, data.Length);
	}
}