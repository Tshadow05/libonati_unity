﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Attach to a gameobject that has a networking class
 * Make sure the networking class notifys this component whenever it recieves a network message by calling recieveNetworkMessage().
*/

public class NetworkDebuggingTool : MonoBehaviour {
	public bool debugging {
		get{
			return enabled;
		}
		set{
			enabled = value;
		}
	}
	[Header("OPTIONS")]
	public bool startOnAwake = true;
	public bool logSending = true;
	public bool logRecieving = false;
	public float debugTestLengthInSeconds = 1;

	[Header("RESULTS")]
	[Header("Sending")]
	public int sentFrames;
	[Header("Recieving")]
	public int numberOfFrames;
	public int numberOfMessage;
	public int framesWithoutAMessage;
	public int messagesWithoutAFrame;
	public float totalFramesWithoutAMessage = 0;
	public int totalFrames = 0;

	private bool hadAMessageThisFrame = false;
	private float timeDebugging;

	void Awake(){
		if (startOnAwake) {
			startDebugTest ();
		}
	}

	private void Update(){
		if (Input.GetKeyDown (KeyCode.D)) {
			startDebugTest ();
		}
		if (debugging) {
			numberOfFrames++;
			if (!hadAMessageThisFrame) {
				framesWithoutAMessage++;
			}
			hadAMessageThisFrame = false;
			timeDebugging += Time.deltaTime;
		}
	}
	public void recieveNetworkMessage(){
		numberOfMessage++;
		if (hadAMessageThisFrame) {
			messagesWithoutAFrame++;
		}
		hadAMessageThisFrame = true;
	}
	public void sendNetworkMessage(){
		sentFrames++;
	}
	public void startDebugTest(){
		//Check for missing frames and extra frames
		//Used to check reliability of messages that should be coming in ever frame
		resetDebugInfo();
		totalFrames = 1;
		totalFramesWithoutAMessage = 1;
		timeDebugging = 0;

		CancelInvoke ("stopDebugTest");

		Debug.Log ("Starting Network Debugging");
		debugging = true;
		InvokeRepeating ("printDebugTest", debugTestLengthInSeconds, debugTestLengthInSeconds);
	}
		
	private void resetDebugInfo(){
		totalFrames += numberOfFrames;
		totalFramesWithoutAMessage += framesWithoutAMessage;

		sentFrames = 0;
		numberOfFrames = 0;
		numberOfMessage = 0;
		framesWithoutAMessage = 0;
		messagesWithoutAFrame = 0;
		hadAMessageThisFrame = false;

	}
	private void stopDebugTest(){
		CancelInvoke ("printDebugTest");
		debugging = false;
	}

	private void printDebugTest(){
		if (!debugging) {
			return;
		}
		string message = "";
		message = "NETWORK DEBUG LOG: \n";
		if (logSending) {
			message += "Sending: " + sentFrames / debugTestLengthInSeconds + " fps";
		}
		if (logRecieving) {
			message += "Number of Frames: " + numberOfFrames + "\n";
			message += "Number of Messages: " + numberOfMessage + "\n";
			message += "Frames Without a Message: " + framesWithoutAMessage + "\n";
			message += "% Frames Without a Message: %" + totalFramesWithoutAMessage / totalFrames * 100 + "\n";
			message += "Messages without a Frame: " + messagesWithoutAFrame;
		}
		Debug.Log (message);

		resetDebugInfo();
	}
}
