﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;
using System;

public class Network_MQTT : MonoBehaviour {
	private MqttClient client;
	private Queue<MQTT_Message> messageQueue;
	private object locker = new object();

	public string ipAddress = "104.236.171.74";
	public int port = 1883;
	public bool readAllMessages = true;//if true messages are saved in a que making sure all messages get read. If false, app will only read latest message

	public delegate void OnRecieveMessage(MQTT_Message messageObject);
	public delegate void OnConnectionChange (bool connected);
	public event OnRecieveMessage onRecieveMessage;
	public event OnConnectionChange onConnectionChange;

	private string[] savedChannels;

	private MQTT_Message lastMessage;
	private NetworkDebuggingTool debugTool;

	MqttClient _client;

	public bool connected{
		get {
			return client.IsConnected;
		}
	}

    void Awake() {
        messageQueue = new Queue<MQTT_Message>();
		debugTool = gameObject.GetComponent<NetworkDebuggingTool> ();
	}
	private void Update(){
		if (!readAllMessages) {
			MQTT_Message m = popMessage ();
			if (m != null && onRecieveMessage != null) {
				onRecieveMessage (m);
			}
			return;
		} else {
			//traverse message queue and interperet
			if(messageQueue.Count > 0){
				MQTT_Message message = messageQueue.Dequeue();

				if (!readAllMessages) {
					messageQueue.Clear ();
				}

				//Debug.Log ("Recieved Message: " + message.message);
				if(onRecieveMessage != null){
					onRecieveMessage(message);
				}
			}
		}
	}
    public void connect()
    {
		try{
			Debug.Log("Connecting MQTT: " + ipAddress + ": " + port);
			CancelInvoke("connect");
	        // create client instance 
	        client = new MqttClient(ipAddress, port, false, null);

	        // register to message received 
	        client.MqttMsgPublishReceived += client_MqttMsgPublishReceived;
			client.MqttMsgDisconnected += client_MqttMsgDisconnected;

	        messageQueue = new Queue<MQTT_Message>();

			string clientId = Guid.NewGuid().ToString(); 
			client.Connect(clientId); 

			if(savedChannels != null){
				subscribe(savedChannels);
			}
			if(client.IsConnected){
				Debug.Log("Connected MQTT");
			}else{
				Debug.Log("Unable to connect to MQTT");
				startReconnect ();
			}
		}catch(System.Exception e){
			Debug.LogError ("Error connecting to MQTT: " + e.Message);
			startReconnect ();
		}
	}
	private void startReconnect(){
		InvokeRepeating ("connect", 5,5);
	}
    public void subscribe(string[] channels)
    {
		if (client != null && client.IsConnected) {
			string log = "Channells: \n";
			byte[] qos = new byte[channels.Length];
			for(int i=0; i<channels.Length; i++){
				log+= channels[i] + "\n";
				qos [i] = MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE;
			}
			Debug.Log (log);
			client.Subscribe (channels, qos);
			savedChannels = null;
		} else {
			savedChannels = channels;
		}
    }
	private void client_MqttMsgDisconnected(object sender, System.EventArgs e){
		Debug.Log ("Lost MQTT Connection");
		if (onConnectionChange != null) {
			onConnectionChange (connected);
		}
	}
	private void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 
		MQTT_Message message = new MQTT_Message (e.Topic, System.Text.Encoding.UTF8.GetString (e.Message));
		if (readAllMessages) {
			messageQueue.Enqueue (message);
		} else {
			pushMessage(message);
		}

		if (debugTool != null && debugTool.debugging) {
			debugTool.recieveNetworkMessage ();
		}
	} 
	public void sendMessage(MQTT_Message message){
		sendMessage (message.topic, message.message);
	}
	public void sendMessage(string Channel, string Payload, bool retain=false){
		//Debug.Log ("Sending Message Channel: " + Channel + " Message: " + Payload);
		if (debugTool != null && debugTool.debugging) {
			debugTool.sendNetworkMessage ();
		}
		client.Publish(Channel, System.Text.Encoding.UTF8.GetBytes(Payload), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, retain);
	}

	private void pushMessage(MQTT_Message message){
		lock (locker) {
			lastMessage = message;
		}
	}
	private MQTT_Message popMessage(){
		lock (locker) {
			MQTT_Message m = lastMessage;
			lastMessage = null;
			return m;
		}
	}


}
