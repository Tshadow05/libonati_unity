﻿using UnityEngine;
using System.Collections;

public class TimedLerp_Basic : MonoBehaviour {
	public enum LerpingMode{
		LINEAR,
		SMOOTH_IN_OUT
	}
	public LerpingMode lerpingMode = LerpingMode.LINEAR;
	private bool animatePosition = false;
	private bool animateScale = false;
	private bool animateRotation = false;
	
	private float startTimePosition = 0;
	private float startTimeScale = 0;
	private float startTimeRotation = 0;
	
	private Vector3 startPosition;
	private Vector3 startScale;
	private Quaternion startRotation;

	private Vector3 targetPosition;
	private Vector3 targetScale;
	private Quaternion targetRotation;
	
	public Transform targetTransform;

	public float positionLengthInSeconds = 1;
	public float scaleLengthInSeconds = 1;
	public float rotationLengthInSeconds = 1;

	public Space transformSpace = Space.Self;
	public bool currentlyLerping {
		get {
			return animateScale || animateRotation || animatePosition;		
		}
	}
	public Vector3 getTargetPosition(){
		return targetPosition;
	}

	public delegate void OnFinishedLerp();
	public OnFinishedLerp onFinishedLerp;

	// Use this for initialization
	void Awake () {
		if(targetTransform == null){
			targetTransform = gameObject.transform;
		}
	}
	
	private Vector3 position{
		get{
			if(transformSpace == Space.World){
				return targetTransform.position;
			}else{
				return targetTransform.localPosition;
			}
		}
		set{
			if(transformSpace == Space.World){
				targetTransform.position = value;
			}else{
				targetTransform.localPosition = value;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		float d = 0;
		if(animatePosition){
			d = (Time.time - startTimePosition) / positionLengthInSeconds;
			if(d>1){
				position = targetPosition;
				animatePosition = false;
				if(onFinishedLerp != null){
					onFinishedLerp ();
				}
			}else{
				position = Vector3.Lerp(startPosition,targetPosition, convertDelta(d));
			}
		}
		if(animateScale){
			d = (Time.time - startTimeScale) / scaleLengthInSeconds;
			if(d>1){
				targetTransform.localScale = targetScale;
				animateScale = false;
				if(onFinishedLerp != null){
					onFinishedLerp ();
				}
			}else{
				targetTransform.localScale = Vector3.Lerp(startScale,targetScale, convertDelta(d));
			}
		}
		if(animateRotation){
			d = (Time.time - startTimeRotation) / rotationLengthInSeconds;
			if(d>1){
				targetTransform.localRotation = targetRotation;
				animateRotation = false;
				if(onFinishedLerp != null){
					onFinishedLerp ();
				}
			}else{
				targetTransform.localRotation = Quaternion.Lerp(startRotation, targetRotation, convertDelta(d));
			}
		}
	}
	private float convertDelta(float d){
		switch(lerpingMode){
			case LerpingMode.LINEAR:
				return d;
			case LerpingMode.SMOOTH_IN_OUT:
				return Mathf.SmoothStep (0f, 1f, d);
		}
		return d;
	}

	public void moveTo(Vector3 newPosition){
		startPosition = position;
		targetPosition = newPosition;
		startTimePosition = Time.time;
		animatePosition = true;
	}
	public void scaleTo(Vector3 newScale){
		startScale = targetTransform.localScale;
		targetScale = newScale;
		startTimeScale = Time.time;
		animateScale = true;
	}
	public void rotateTo(Quaternion newRotation){
		startRotation = targetTransform.localRotation;
		targetRotation = newRotation;
		startTimeRotation = Time.time;
		animateRotation = true;
	}
}
