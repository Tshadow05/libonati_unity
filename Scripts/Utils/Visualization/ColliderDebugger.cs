﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wild {
    public class ColliderDebugger : MonoBehaviour {
        public new Collider collider;
        public MeshFilter filter;
        public MeshRenderer ren;

        private void LateUpdate() {
            BoxCollider boxCollider = collider as BoxCollider;
            if (boxCollider) {
                filter.gameObject.transform.localPosition = boxCollider.center;
                filter.gameObject.transform.localScale = boxCollider.size;
            }
        }

        public void SetColor(Color newColor) {
            if ( ren.material.HasProperty( "_Color" )) {
                newColor.a = ren.material.color.a;
                ren.material.color = newColor;
            } else if ( ren.material.HasProperty( "_TintColor" )) {
                newColor.a = ren.material.GetColor("_TintColor").a;
                ren.material.SetColor("_TintColor", newColor);
            }
        }
    }

}