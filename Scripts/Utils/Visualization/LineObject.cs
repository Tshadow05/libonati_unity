﻿using UnityEngine;
using System.Collections;

namespace Libonati{
	public class LineObject : MonoBehaviour {
		private Vector3 p1;
		private Vector3 p2;

		public GameObject mesh;
		public Transform endT;
		public Transform autoFollowStartT;
		public Transform autoFollowTargetT;

		public float worldDistance{
			get{
				return Vector3.Distance (p1, p2);
			}
		}
		public float localDistance{
			get{
				return Vector3.Distance (transform.InverseTransformPoint(p1), transform.InverseTransformPoint(p2));
			}
		}
		public Vector3 getP1(){
			return p1;
		}
		public Vector3 getP2(){
			return p2;
		}

		void LateUpdate(){
			if(autoFollowStartT != null && autoFollowTargetT != null){
				setLine(autoFollowStartT.position, autoFollowTargetT.position);
			}
		}
		public void setLine(Vector3 startPos, Vector3 endPos){
			setLine (startPos, endPos, Vector3.up);
		}
		public void setLine(Vector3 startPos, Vector3 endPos, Vector3 upVec){
			p1 = startPos;
			p2 = endPos;
			transform.position = startPos;
			transform.LookAt (endPos, upVec);
			Vector3 newLocalScale = mesh.transform.localScale;
			newLocalScale.z = localDistance;
			mesh.transform.localScale = newLocalScale;

			// Debug.Log("LINE START: " + startPos);
			// Debug.Log("LINE END: " + endPos);
			// Debug.Log("LINE distance: " + localDistance);
			// Debug.Log("LINE local scale: " + mesh.transform.localScale);

			if (endT) {
				endT.position = endPos;
				//endT.LookAt (startPos);
			}
		}

	}
}