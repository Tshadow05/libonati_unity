﻿using UnityEngine;
using System.Collections;

namespace Libonati{
	public class LineArc : MonoBehaviour {
		public LineRenderer line;

		[Range(0,1000)]
		public int resolution = 10;
		public float arcHeight = 45;
		public float arcGravity{
			get{
				return Physics.gravity.y;
			}
		}
		public bool arcLocally = false;

		public bool drawSegment = false;
		public float startDrawPoint = 0;
		public float endDrawPoint = 32;

		[Header("Debugging")]
		public bool debug = false;
		private Transform targetT;

		private Color _color = Color.white;
		public Color color{
			set{
				_color = value;
				if(line != null){
					// Line color is fed to shader as vertex colors
					// May be an oversight on the unity side, but these are not being linearized by the engine, so doing it manually here
					Color linColor = value.linear;
					line.startColor = linColor;
					line.endColor = linColor;
				}
			}
			get{
				return _color;
			}
		}

		void Awake(){
            line.positionCount = resolution;
			targetT = new GameObject ("targetT").transform;
			targetT.SetParent (transform, false);
		}
			
		void Update(){
			if (debug) {
				line.positionCount = resolution;
				calculateArc (transform.position, targetT.position);
			}
		}
		
		public void calculateArc(Vector3 startPosition, Vector3 endPosition){
			if(!enabled || targetT == null){
				return;
			}
			color = _color;
			Vector3[] points;
			if (drawSegment) {
				points = new Vector3[(int)endDrawPoint - (int)startDrawPoint];
				for (int i = (int)startDrawPoint; i < (int)endDrawPoint; i++) {
					points [i - (int)startDrawPoint] = sampleParabola (startPosition, endPosition, arcHeight, (float)i / (resolution - 1));
				}

				line.positionCount = (int)endDrawPoint - (int)startDrawPoint;
			} else {
				points = new Vector3[resolution];
				for (int i = 0; i < points.Length; i++) {
					points [i] = sampleParabola (startPosition, endPosition, arcHeight, (float)i / (resolution - 1));
				}
			}
			line.SetPositions (points);
			targetT.position = endPosition;
		}

		Vector3 sampleParabola ( Vector3 start, Vector3 end, float height, float t ) {
			if ( Mathf.Abs( start.y - end.y ) < 0.05f ) {
				//start and end are roughly level, pretend they are - simpler solution with less steps
				Vector3 travelDirection = end - start;
				Vector3 result = start + t * travelDirection;
				result.y += Mathf.Sin( t * Mathf.PI ) * height;
				return result;
			} else {
				//start and end are not level, gets more complicated
				Vector3 travelDirection = end - start;
				Vector3 levelDirection = end - new Vector3( start.x, end.y, start.z );
				Vector3 right = Vector3.Cross( travelDirection, levelDirection );
				Vector3 up = Vector3.Cross( right, travelDirection );
				if(!arcLocally){
					up = Vector3.Cross(right, levelDirection);
				}
				if ( end.y > start.y ) up = -up;
				Vector3 result = start + t * travelDirection;

				result += ( Mathf.Sin( t * Mathf.PI ) * height ) * up.normalized;
				return result;
			}
		}
	}
}
