﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Wild {
    public class PortalDebugger : MonoBehaviour {
        public PortalVC portal;
        public PortalGrabbable grabbable;
        public GameObject grabbableDebug;
        public ColliderDebugger jumpableDebug;

        private void Start() {
            jumpableDebug.gameObject.SetActive(true);
        }

        private void LateUpdate() {
            grabbableDebug.SetActive(grabbable.dragging);
            jumpableDebug.SetColor(portal.isEntrance ? Color.green : Color.red);
        }
    }

}