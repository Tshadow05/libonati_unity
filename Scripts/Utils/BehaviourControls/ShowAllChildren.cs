﻿using UnityEngine;
using System.Collections;

namespace Libonati{
	public class ShowAllChildren : MonoBehaviour {
		public bool showOnEnable = true;
		public bool hideOnDisable = false;
		public float showDelayInSeconds = 0;

		void OnEnable(){
			if (showOnEnable) {
				Debug.Log ("Showing all");
				Invoke ("showAll", showDelayInSeconds);
			}
		}
		void OnDisable(){
			if (hideOnDisable) {
				Debug.Log ("Disableing all");
				hideAll ();
			}
		}

		private void showAll(){
			LibonatiHelper.showAll (gameObject);
		}
		private void hideAll(){
			LibonatiHelper.hideAll (gameObject);
		}
	}
}