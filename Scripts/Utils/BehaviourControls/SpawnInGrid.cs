﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Libonati {
    public class SpawnInGrid : MonoBehaviour {
        public LibonatiHelper.EventState spawnOn = LibonatiHelper.EventState.NONE;
        public GameObject template;

        public int maxCount = -1;
        public Vector2i numberOfSlots = new Vector2i(5, 5);
        public Vector2 startOffset = Vector2.zero;
        public Vector2 spacingSize = Vector2.one;
        public bool autoCenter = true;
        public bool scaleItems = false;

        public delegate void OnSlotSpawn(int i, GameObject obj);

        public event OnSlotSpawn onSlotSpawn;

        private Vector2i currentGridPosition = new Vector2i();

        private Vector3 startPosition {
            get {
                if(autoCenter) {
                    return new Vector3(startOffset.x + spacingSize.x / 2f - numberOfSlots.x * spacingSize.x / 2f, startOffset.y + spacingSize.x / 2f - numberOfSlots.y * spacingSize.y / 2f, 0);
                } else {
                    return new Vector3(startOffset.x, startOffset.y, 0);
                }
            }
        }

        void Awake() {
            if(spawnOn == LibonatiHelper.EventState.AWAKE) {
                spawn();
            }
        }

        void Start() {
            if(spawnOn == LibonatiHelper.EventState.START) {
                spawn();
            }
        }

        public void spawn() {
            int x = 0;
            int y = 0;
            Vector3 newPosition = startPosition;
            int max = maxCount;
            if(max < 0) {
                max = numberOfSlots.x * numberOfSlots.y;
            }
            currentGridPosition = new Vector2i();
            for(int i = 0; i < max; i++) {
                GameObject newObject = (GameObject)Instantiate(template, transform);
                PlaceInGrid(newObject.transform);
                if(onSlotSpawn != null) {
                    onSlotSpawn(i, newObject);
                }
                x++;
                newPosition.x += spacingSize.x;
                if(x >= numberOfSlots.x) {
                    //End of rows
                    x = 0;
                    newPosition.x = startPosition.x;
                    y++;
                    newPosition.y += spacingSize.y;
                    if(y >= numberOfSlots.y) {
                        //End of Columns
                    }
                }
            }
        }

        public bool PlaceInGrid(Transform targetTransform){
            int nextX = currentGridPosition.x + 1;
            int nextY = currentGridPosition.y;
            if(nextX >= numberOfSlots.x){
                nextX = 0;
                nextY++;
            }
            return PlaceInGrid(targetTransform, nextX, nextY);
        }

        public bool PlaceInGrid(Transform targetTransform, int slotX, int slotY){
            if(slotX >= numberOfSlots.x || slotY >= numberOfSlots.y){
                return false;
            }
            targetTransform.SetParent(transform, false);
            if(scaleItems) {
                Vector3 scaleOffset = new Vector3(spacingSize.x, spacingSize.y, spacingSize.x);
                Vector3 newScale = targetTransform.localScale;
                newScale.x *= scaleOffset.x;
                newScale.y *= scaleOffset.y;
                newScale.z *= scaleOffset.z;
                targetTransform.localScale = newScale;
            }
            Vector3 newPosition = new Vector3(
                startPosition.x + (slotX * spacingSize.x),
                startPosition.y + (slotY * spacingSize.y), 0);
            targetTransform.localPosition = newPosition;
            targetTransform.localRotation = Quaternion.identity;
            currentGridPosition = new Vector2i(slotX, slotY);
            return true;
        }

        public void ClearGrid(){
            foreach(Transform t in transform){
                Destroy(t.gameObject);
            }
            currentGridPosition = new Vector2i(0,0);
        }

        public void OnDrawGizmos() {
            Vector3 newPosition = startPosition;
            Vector3 newSize = new Vector3(spacingSize.x * .98f, spacingSize.y * .98f, .1f);
            Gizmos.color = new Color(.5f, .3f, .3f, .5f);
            int max = maxCount;
            if(max < 0) {
                max = numberOfSlots.x * numberOfSlots.y;
            }

            int x = 0;
            int y = 0;
            for(int i = 0; i < max; i++) {
                Gizmos.DrawMesh(PrimitivePlus.PrimitivePlusMeshes.GetMeshType(PrimitivePlus.PrimitivePlusType.Cube), transform.TransformPoint(newPosition), transform.rotation, newSize);
                x++;
                newPosition.x += spacingSize.x;
                if(x >= numberOfSlots.x) {
                    //End of rows
                    x = 0;
                    newPosition.x = startPosition.x;
                    y++;
                    newPosition.y += spacingSize.y;
                    if(y >= numberOfSlots.y) {
                        //End of Columns
                    }
                }
            }
        }
    }
}
