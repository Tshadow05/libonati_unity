﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardTransform : MonoBehaviour {
	public enum LookAtMode{
		NONE,
		SPHERICAL
	}
	public LookAtMode lookAtMode = LookAtMode.SPHERICAL;
	public Transform targetEye;

	// Use this for initialization
	void Start () {
		if(targetEye == null){
			targetEye = UnityEngine.Camera.main.transform;
		}
	}
	
	// Update is called once per frame
	void Update () {
		switch(lookAtMode){
			case LookAtMode.NONE:
				return;
			case LookAtMode.SPHERICAL:
				transform.LookAt (targetEye.position);
				return;
		}
	}
}
