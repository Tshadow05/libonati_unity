﻿using UnityEngine;
using System.Collections;

public class FollowTransform : MonoBehaviour {
    public Transform target;

    public enum FollowMode {
        INSTANT,
        EASE_OUT
    }

    public FollowMode followMode = FollowMode.INSTANT;
    public float lerpMod = 1;
    public bool followPosition = true;
    public bool followRotation = true;
    public bool followScale = false;
    public bool globalScale = false;

    private Vector3 targetScale {
        get {
            if (globalScale) {
                return Vector3.one * target.lossyScale.y / (transform.parent ? transform.parent.lossyScale.y : 1.0f);
            }
            return target.localScale;
        }
    }

    // Update is called once per frame
    void LateUpdate() {
        switch(followMode) {
            case FollowMode.INSTANT:
                if(followPosition) transform.position = target.position;
                if(followRotation) transform.rotation = target.rotation;
                if (followScale) transform.localScale = targetScale;
                break;
            case FollowMode.EASE_OUT:
                if(followPosition) transform.position = Vector3.Lerp(transform.position, target.position, Time.deltaTime * lerpMod);
                if(followRotation) transform.rotation = Quaternion.Lerp(transform.rotation, target.rotation, Time.deltaTime * lerpMod);
                if(followScale) transform.localScale = Vector3.Lerp(transform.localScale, targetScale, Time.deltaTime * lerpMod);
                break;
        }
    }
}
