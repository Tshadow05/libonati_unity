﻿using UnityEngine;
using System.Collections;
using System;

public class TimeOfDay : MonoBehaviour {

	[Header("Components")]
	public Light sun;
	public Transform sunModel;

	[Header("Settings")]
	public float sunExposure = 1f;
	public float moonExposure = .1f;
	public float zOffset = -.25f;
	[Header("Optional")]
	public TextClock textDisplay;

	private float currentDayPercent = .25f;

	void Awake(){
		updateSun ();
	}

	void Update(){
		if(Input.GetKey(KeyCode.RightArrow)){
			setTime (currentDayPercent + 1*Time.deltaTime);
		}else if(Input.GetKey(KeyCode.LeftArrow)){
			setTime (currentDayPercent - 1*Time.deltaTime);
		}

		if (textDisplay != null) {
			DateTime dt = new DateTime(0);
			dt += time;
			textDisplay.setClock(dt);
		}
	}

	public void updateSun(){
		setTime (currentDayPercent);
	}

	//A percentage value of day/night cycle;  morning 0 -> night .5f -> morning 1
	public void setTime(float percent){
		percent = percent % 1;
		//Debug.Log ("Setting sun to " + percent + "%");

		//MODIFY GRAPHICS
		currentDayPercent = percent;
		Vector3 newSunPosition = Libonati.LibonatiHelper.findPositionOnCircle(Vector3.zero, 1, percent*360) ;
		newSunPosition.z = zOffset;
		sunModel.localPosition = newSunPosition;
		sunModel.LookAt (transform.position);

		//APPLY SETTINGS
		if (sun != null) {
			sun.transform.localRotation = sunModel.localRotation;
		}
		if (percent > .5f) {
			RenderSettings.skybox.SetFloat ("_Exposure", moonExposure);
		} else {
			RenderSettings.skybox.SetFloat ("_Exposure", sunExposure);
		}
	}

	//7am sunrise - 7pm sunset
	public void setTime(TimeSpan time){
		int hour = time.Hours;
		hour -= 7;
		if (hour < 0) {
			hour = 24 + hour;
		}
		setTime (hour / 24f);
	}

	public TimeSpan time{
		get{
			return  new TimeSpan (((int)(Mathf.Abs(currentDayPercent*24f)) + 7)%24, 0, 0);
		}
	}
}
