﻿using UnityEngine;
using System.Collections;

namespace Libonati{
	public class SetParent : MonoBehaviour {
		public Transform newParent;


		public LibonatiHelper.EventState enableOn = LibonatiHelper.EventState.AWAKE;

		// Use this for initialization
		void Awake(){
			if (enableOn == LibonatiHelper.EventState.AWAKE) {
				set ();
			}
		}
		void Start(){
			if (enableOn == LibonatiHelper.EventState.START) {
				set ();
			}
		}

		public void set(){
			transform.parent = newParent;
		}
	}
}