﻿using UnityEngine;
using System.Collections;

public class EnableOnAwake : MonoBehaviour {
	public GameObject target;
	public bool enable = true;
	// Use this for initialization
	void Awake () {
		target.SetActive (enable);
	}
}
