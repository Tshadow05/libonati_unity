﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Libonati{
	public class RigidConstrainLocalAxis : MonoBehaviour {
		[Header("Freeze Local Position")]
		public bool lockPositionX = false;
		public bool lockPositionY = false;
		public bool lockPositionZ = false;
		[Header("Freeze Local Rotation")]
		public bool lockRotationX = false;
		public bool lockRotationY = false;
		public bool lockRotationZ = false;

		private Rigidbody rigid;
		private Vector3 savedPosition = Vector3.zero;
		private Vector3 savedRotation= Vector3.zero;

		void Start(){
			rigid = gameObject.GetComponent<Rigidbody>();
			SaveTransform();
		}

		void LateUpdate () {
			if(rigid == null){
				return;
			}
			if(rigid.isKinematic){
				SaveTransform();
				return;
			}

			if(lockPositionX || lockPositionY || lockPositionZ){
				Vector3 newLocalPosition = new Vector3(
					lockPositionX ? savedPosition.x : transform.localPosition.x,
					lockPositionY ? savedPosition.y : transform.localPosition.y,
					lockPositionZ ? savedPosition.z : transform.localPosition.z);

				//Use vel to preserve physics
				//rigid.velocity = (transform.parent.TransformPoint(newLocalPosition) - transform.position) / Time.deltaTime;
				//Setting transform will break physics but forces object into a true position
				transform.localPosition = newLocalPosition;
			}

			if(lockRotationX || lockRotationY || lockRotationZ){
				Vector3 newLocalEulerAngles = new Vector3(
					lockRotationX ? savedRotation.x : transform.localEulerAngles.x,
					lockRotationY ? savedRotation.y : transform.localEulerAngles.y,
					lockRotationZ ? savedRotation.z : transform.localEulerAngles.z);

				//Use vel to preserve physics
				//rigid.angularVelocity = (transform.parent.TransformDirection(newLocalEulerAngles) - transform.eulerAngles) / Time.deltaTime;
				//Setting transform will break physics but forces object into a true position
				transform.localEulerAngles = newLocalEulerAngles;
			}
		}

		private void SaveTransform(){
			savedPosition = transform.localPosition;
			savedRotation = transform.localEulerAngles;
		}
	}

}