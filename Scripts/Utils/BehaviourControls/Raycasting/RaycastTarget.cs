﻿using UnityEngine;
using System.Collections;

namespace Libonati{
	public class RaycastTarget : MonoBehaviour {
		[HideInInspector]
		public bool inRayFocus = false;
		public virtual void onRayEnter(RaycastHit hitInfo, GameObject owner){
			//Debug.Log ("Ray Enter");
			inRayFocus = true;
		}
		public virtual void onRayStay(RaycastHit hitInfo, GameObject owner){
			//Debug.Log ("Ray Stay");
		}
		public virtual void onRayLeave(RaycastHit hitInfo, GameObject owner){
			//Debug.Log ("Ray Leave");
			inRayFocus = false;
		}
	}
}