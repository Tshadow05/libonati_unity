﻿using UnityEngine;
using System.Collections;

namespace Libonati{
	public class Raycasting : MonoBehaviour {
		public Transform aimingTransform;
		public LayerMask layerMask = -1;
		public float maxDistance = 1000;
		public LineObject optionalLine;
		public bool onlyShowWhenInRange = false;
		private bool hide = false;


		private RaycastHit hitInfo = new RaycastHit();
		private RaycastTarget target;
		private Vector3 hitPoint;
		public Vector3 lastHitPosition {
			get {
				return hitPoint;
			}
		}
		public RaycastHit lastHitInfo {
			get {
				return hitInfo;
			}
		}

		void Start(){
			if (aimingTransform == null) {
				aimingTransform = transform;	
			}
		}

		// Update is called once per frame
		void Update () {
			//Cast Ray
			hitPoint = aimingTransform.position + aimingTransform.forward.normalized * maxDistance;
			RaycastTarget newTarget = null;
			if (Physics.Raycast (aimingTransform.position, aimingTransform.forward, out hitInfo, maxDistance, layerMask)) {
				hitPoint = hitInfo.point;
				newTarget = hitInfo.collider.gameObject.GetComponentInChildren<RaycastTarget> ();
				if (newTarget != null) {
					if (newTarget != target) {
						//Debug.Log ("newTarget");
						if (target != null && target.inRayFocus) {
							target.onRayLeave (hitInfo, gameObject);
						}
						newTarget.onRayEnter (hitInfo, gameObject);
					} else {
						newTarget.onRayStay (hitInfo, gameObject);
					}
				}
			} else {
				if (target != null && target.inRayFocus) {
					target.onRayLeave (hitInfo, gameObject);
				}
				//target = null;
			}

			if (newTarget == null && target != null && target.inRayFocus) {
				target.onRayLeave (hitInfo, gameObject);
			}
			target = newTarget;

			if (optionalLine != null) {
				if ((onlyShowWhenInRange && target == null) || hide) {
					optionalLine.gameObject.SetActive (false);
				} else {
					optionalLine.gameObject.SetActive (true);
				}
				optionalLine.setLine (aimingTransform.position, hitPoint);
			}
		}

		void OnDrawGizmos(){
			Gizmos.color = Color.magenta;
			Gizmos.DrawLine (aimingTransform.position, hitPoint);
		}

		public void hideRay(){
			hide = true;
		}
		public void showRay(){
			hide = false;
		}
	}
}