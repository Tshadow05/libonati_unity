﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class VariableWobbler{
	public delegate void OnVariableUpdate(float newValue);
	public event OnVariableUpdate onVariableUpdate;

	public float timeMod = 1;
	public float smoothingMod = 1;
	public float rangeMin = 0;
	public float rangeMax = 1;
	public float speedMod = 10f;
	public float rateOfChange = 1f;
	public float rateOfDirChange = .1f;

	private float rawValue;
	private float value;
	private float lastValue;
	private float rangeDistance;
	private float dirMod = 1;

	private bool inBounds = true;
	private bool dirChangeing = false;
	private float dirVel = .1f;

	private float velocity = 0;
	public float vel {
		get {
			return velocity;
		}
	}
	public float valueRaw{
		get{
			return rawValue;
		}
	}
	public float valueSmooth{
		get{
			return this.value;
		}
	}


	public VariableWobbler(){
		rangeDistance = rangeMax - rangeMin;
		value = rangeDistance / 2f + rangeMin;
	}

	public void setValue(float newValue){
		value = Mathf.Clamp(newValue,rangeMin,rangeMax);
		rawValue = value;
	}
	
	// Update is called once per frame
	public void Update () {
		if (dirChangeing) {
			dirMod += dirVel * Time.deltaTime * timeMod;
			if(dirMod > 1){
				dirMod = 1;
				dirChangeing = false;
			}
			if(dirMod < -1){
				dirMod = -1;
				dirChangeing = false;
			}
		}
		if (Random.Range (0f, 1f) > 1 - rateOfChange) {
			velocity = Random.Range(0,speedMod*dirMod) * Time.deltaTime;
			rawValue += velocity * timeMod;
			if(rawValue > rangeMax-rangeDistance*.2f || rawValue < rangeMin+rangeDistance*.2f){
				//rawValue = Mathf.Clamp(rawValue,rangeMin,rangeMax);
				if(inBounds){
					inBounds = false;
					changeDir();
				}
			}else{
				inBounds = true;
			}
			if(Random.Range(0f,1f) > 1-rateOfDirChange){
				changeDir();
			}
		}

		value = Mathf.Lerp(value, rawValue, Time.deltaTime * smoothingMod * timeMod);

		if(value != lastValue){
			lastValue = value;
			if(onVariableUpdate != null){
				onVariableUpdate(value);
			}
		}
	}

	private void changeDir(){
		if (!dirChangeing) {
			dirVel *= -1;
			dirChangeing = true;
		}
	}
}
