﻿using UnityEngine;
using System.Collections;

public class SinWave : MonoBehaviour {
	private static int WaveCount = 0;

	private ParticleSystem pSystem;
	private ParticleSystem.Particle[] particles;
	private float distanceBetweenParticles;
	private float clampDistance = 1;

	[Header("Init Settings")]
	public int numberOfParticles = 1000;
	public float waveDistance = 100;
	public Gradient color;
	[Header("Runtime Settings")]
	public float particleSize = 1;
	public float amplitude = 10;
	public float waveSpeed = 1;
	public float wavelength = 1;
	public float clampSpeedMod = 1;
	public float clampOffset = 0;

	public VariableWobbler[] wobbles;

	// Use this for initialization
	void Awake () {
		WaveCount++;
		pSystem = gameObject.GetComponent<ParticleSystem> ();
		particles = new ParticleSystem.Particle[numberOfParticles];

		distanceBetweenParticles = waveDistance/numberOfParticles;
		Vector3 newPosition = new Vector3 (-numberOfParticles/2*distanceBetweenParticles, 0, 0);
		for (int i=0; i<numberOfParticles; i++) {
			particles[i] = new ParticleSystem.Particle();
			particles[i].position = newPosition;
			particles[i].startSize = 1;
			particles[i].startColor = color.Evaluate(i/(float)numberOfParticles);
			newPosition.x += distanceBetweenParticles;
		}

		int numberOfWobbles = 3;
		wobbles = new VariableWobbler[numberOfWobbles];
		for (int i=0; i<numberOfWobbles; i++) {
			WobbleVisualizer vis = new GameObject("Wobble").AddComponent<WobbleVisualizer>();
			vis.transform.SetParent(transform, false);
			vis.transform.localPosition = new Vector3(WaveCount,i * -3f - WaveCount,0);

			wobbles[i] = new VariableWobbler();
			vis.wobble = wobbles[i];
			
			wobbles[i].smoothingMod = .11f;
			wobbles[i].rateOfChange = .05f;
			wobbles[i].rateOfDirChange = .01f;
		}
		
		//WAVELENGTH
		wobbles [0].onVariableUpdate += updateWavelength;
		wobbles [0].rangeMin = wavelength - wavelength * .9f;
		wobbles [0].rangeMax = wavelength + wavelength * .4f;
		wobbles [0].setValue (wavelength);
		wobbles [0].speedMod = 100f;
		
		//WAVE SPEED
		wobbles [1].onVariableUpdate += updateWaveSpeed;
		wobbles [1].rangeMin = waveSpeed - waveSpeed * .9f;
		wobbles [1].rangeMax = waveSpeed + waveSpeed * .3f;
		wobbles [1].setValue (waveSpeed);
		wobbles [1].speedMod = 2f;

		//AMPLITUDE
		wobbles [2].onVariableUpdate += updateAmplitude;
		wobbles [2].rangeMin = amplitude - amplitude * .3f;
		wobbles [2].rangeMax = amplitude + amplitude * .3f;
		wobbles [2].setValue (amplitude);
		wobbles [2].speedMod = 5f;
	}
	
	// Update is called once per frame
	void Update () {
		clampDistance = 1-(Mathf.Sin ((Time.time+clampOffset) * clampSpeedMod)+1)/2.5f;
		updateParticles ();
		updateWobbles ();
	}

	private void updateParticles(){
		for (int i=0; i<numberOfParticles; i++) {
			float x = (Time.time * waveSpeed) + (-amplitude/2 + i) / wavelength; 
			float d = Mathf.Abs(numberOfParticles/2f - i)/numberOfParticles*2f/clampDistance;
			//ORIGINAL: 
			//float easeDelta = MathFX.SmoothStep((d),0,1);
			//GRADUAL WOBBLE:
			float easeDelta = MathFX.SmoothStep((d),0,1) * MathFX.SmoothStep((1-d),0,1) * 2;
			//SPLIT
			//float easeDelta = MathFX.SmoothStep((d),0,1) * MathFX.SmoothStep((d),0,1) / 2;
			float y = Mathf.Sin(x)*amplitude*easeDelta;
			
			particles[i].startSize = particleSize;
			particles[i].position = new Vector3(particles[i].position.x, y,0);
		}
		pSystem.SetParticles (particles, numberOfParticles);
	}
	private void updateWobbles(){
		for (int i=0; i<wobbles.Length; i++) {
			wobbles[i].Update();
		}
	}
	
	private void updateWavelength(float newValue){
		if (newValue == 0) {
			return;
		}
		wavelength = newValue;
	}
	private void updateWaveSpeed(float newValue){
		waveSpeed = newValue;
	}
	private void updateAmplitude(float newValue){
		amplitude = newValue;
	}
}
