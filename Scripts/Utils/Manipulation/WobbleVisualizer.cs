﻿using UnityEngine;
using System.Collections;

public class WobbleVisualizer : MonoBehaviour {
	public VariableWobbler wobble;

	public float scale = 1;

	void OnDrawGizmos(){
		Vector3 start = new Vector3(wobble.rangeMin,0,0) + transform.position;
		Vector3 end = new Vector3(wobble.rangeMax,0,0) + transform.position;
		Vector3 center = new Vector3(wobble.valueRaw,0,0) + transform.position;
		Vector3 centerLerp = new Vector3(wobble.valueSmooth,0,0) + transform.position;
		Vector3 vel = new Vector3(wobble.vel,0,0);

		Gizmos.color = Color.white;
		Gizmos.DrawLine (start + new Vector3 (0, scale/2f, 0), start + new Vector3 (0, -scale/2f, 0));
		Gizmos.DrawLine (end + new Vector3 (0, scale/2f, 0), end + new Vector3 (0, -scale/2f, 0));
		Gizmos.DrawLine (start, end);

		Gizmos.color = Color.green;
		Gizmos.DrawLine (center, center+vel);

		Gizmos.color = Color.red;
		Gizmos.DrawSphere (center, scale);
		Gizmos.color = Color.blue;
		Gizmos.DrawSphere (centerLerp, scale/2);
	}
}
