﻿//Date Formatting:
//https://msdn.microsoft.com/en-us/library/8kb3ddd4(v=vs.110).aspx

using UnityEngine;
using System.Collections;
using System;

public class TextClock : MonoBehaviour {
	public TextMesh timeTxt;
	public TextMesh dateTxt;
	
	public string timeFormat = "hh:mmtt";
	public string dateFormat = "ddd M/d/yy";

	public enum UpdateRate
	{
		EVERY_FRAME,
		EVERY_SECOND,
		EVERY_MINUTE,
		EVERY_HOUR, 
		NEVER
	}
	public UpdateRate updateRate = UpdateRate.EVERY_FRAME;

	private DateTime date;

	// Use this for initialization
	void Awake () {
		date = System.DateTime.Now;
		startClock ();
	}
	void Start(){
	}
	public void startClock(){
		if (updateRate == UpdateRate.NEVER) {
			return;
		}

		float updateDelay = 0;
		float updatePause = 0;

		switch (updateRate) {
		case UpdateRate.EVERY_FRAME:
			return;
		case UpdateRate.EVERY_SECOND:
			updateDelay = 1;
			updatePause = 1000 - date.Millisecond;
			break;
		case UpdateRate.EVERY_MINUTE:
			updateDelay = 60;
			updatePause = 60 - date.Second;
			break;
		case UpdateRate.EVERY_HOUR:
			updateDelay = 3600;
			updatePause = 3600 - date.Minute*60;
			break;
		}

		//Initial Update
		updateDateTime();
		//Waiting until the time changes and then start updating at the specified time
		InvokeRepeating ("updateDateTime", updatePause, updateDelay);
	}
	public void stopClock(){
		CancelInvoke ("updateDateTime");
	}
	public void setClock(DateTime dateTime){
		if (timeTxt != null) {
			//Update Time
			timeTxt.text = dateTime.ToString(timeFormat);
		}
		if (dateTxt != null) {
			//Update Date
			dateTxt.text = dateTime.ToString(dateFormat);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (updateRate == UpdateRate.EVERY_FRAME) {
			updateDateTime();
		}
	}

	private void updateDateTime(){
		setClock (DateTime.Now);
	}
}
