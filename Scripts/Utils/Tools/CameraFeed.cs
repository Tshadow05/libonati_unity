﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Libonati{
	public class CameraFeed : MonoBehaviour {
		public Vector2i preferredResolution = new Vector2i(1280,720);
		[HideInInspector]
		public WebCamTexture texture;
		public Renderer meshRenderer;
		public RawImage fullscreenRenderer;
		public bool enableOnStart = true;
		public bool fullscreen = false;

		// Use this for initialization
		void Start () {
			if (meshRenderer == null) {
				meshRenderer = gameObject.GetComponentInChildren<Renderer> ();
			}
			if (fullscreenRenderer == null) {
				fullscreenRenderer = gameObject.GetComponentInChildren<RawImage> ();
			}

			if (enableOnStart) {
				enableCamera ();
			}
		}

		public void enableCamera(){
			if (WebCamTexture.devices == null || WebCamTexture.devices.Length <= 0) {
				Debug.LogWarning ("No Camera Device Found");
				return;
			}

			string deviceName = WebCamTexture.devices [0].name;
			texture = new WebCamTexture (deviceName, preferredResolution.x, preferredResolution.y);
			texture.Play ();

			if (meshRenderer) {
				meshRenderer.material.mainTexture = texture;
			}else if (fullscreenRenderer) {
				//fullscreenRenderer.pixelInset = new Rect (0, 0, Screen.width, Screen.height);
				fullscreenRenderer.texture = texture;
			}else {
				Debug.LogWarning ("No Camera Renderer to display Camera Feed");
			}
		}

		public void disableCamera(){
			if (texture != null) {
				texture.Stop ();
			}
			texture = null;
		}
	}
}