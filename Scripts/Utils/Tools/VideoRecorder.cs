﻿using UnityEngine;
using System.Collections;

public class VideoRecorder : TakeScreenshot {
	[Header("Video")]
	public int numberOfFrames = 600;//10 seconds at 60 fps
	private int i=0;

	public const float maxAttemptTimeInSeconds = 10;
	// Use this for initialization
	void Start () {
	
	}
	public override string ScreenShotName(int width, int height) {
		return FolderName + fileName + string.Format("_{0}x{1}_{2}.png", 
		                                             width, height, 
		                                             i.ToString(padding));
	}
	private string padding{
		get{
			return "D" + Mathf.Floor(Mathf.Log10(numberOfFrames)+1).ToString();
		}
	}
	// Update is called once per frame
	override protected void LateUpdate () {
		if(Input.GetKeyDown(key)){
			startRecording();
		}
	}
	public void startRecording(){
		shouldSaveCaptures = true;
		StartCoroutine (beginRecording());
	}
	private IEnumerator beginRecording(){
		Debug.Log ("Started Video Recording");
		TimeController.FPS = 60;
		float startTime = Time.time;
		for (i=0; i<numberOfFrames; i++) {
			startTime = Time.time;
			yield return StartCoroutine(screenshot());
			if((Time.time - startTime) > maxAttemptTimeInSeconds){
				yield return null;
			}
		}
		TimeController.FPS = -1;
		Debug.Log ("Finished Video Recording");
	}
}
