﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class TakeScreenshot : MonoBehaviour {
	public int imageWidth = 1280;
	public int imageHeight = 720;
	private const int resDepth = 24;
	private Texture2D screenShot;
	private byte[] bytes;
	public byte[] lastScreenshotBytes {
		get {
			return bytes;
		}
	}
	public string path = "";
	public string fileName = "screen";
	public KeyCode key = KeyCode.None;
	public Camera screenshotCamera;
	public bool shouldSaveCaptures = true;
	
	public Texture2D picture {
		get {
			return screenShot;
		}
	}
	
	public virtual string ScreenShotName(int width, int height) {
		return FolderName + fileName + string.Format("_{0}.png", System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
	}
	public string FolderName{
		get{
			if (path == "") {
				return string.Format ("{0}/screenshots/", Application.persistentDataPath);//System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal));
			} else {
				return path + "\\";
			}
		}
	}
	private RenderTexture rt;
	public delegate void OnPictureTaken();
	public OnPictureTaken onPictureTaken;
	
	// Use this for initialization
	void Awake () {
		if (!Directory.Exists (FolderName)) {
			Directory.CreateDirectory(FolderName);
		}
		if (screenshotCamera == null) {
			screenshotCamera = gameObject.GetComponent<Camera> ();
		}
		screenShot = new Texture2D(imageWidth, imageHeight, TextureFormat.RGB24, false);
	}
	protected virtual void LateUpdate() {
		if (Input.GetKeyDown (key)) {
			takeScreenshot (shouldSaveCaptures);
		}
	}
	
	public void takeScreenshot(bool savePicture) {
		shouldSaveCaptures = savePicture;
		StartCoroutine (screenshot());
	}
	
	protected IEnumerator screenshot(){
		yield return new WaitForEndOfFrame();
		
		try{
			rt = new RenderTexture(imageWidth, imageHeight, 24);
			screenshotCamera.targetTexture = rt;

			screenshotCamera.Render();
			RenderTexture.active = rt;
			screenShot.ReadPixels(new Rect(0, 0, imageWidth, imageHeight), 0, 0);
			screenshotCamera.targetTexture = null;
			RenderTexture.active = null; // JC: added to avoid errors
			Destroy(rt);
			
			bytes = screenShot.EncodeToPNG();
			string filename = ScreenShotName(imageWidth, imageHeight);
			
			if(path != "" && shouldSaveCaptures){
				System.IO.File.WriteAllBytes(filename, bytes);
				Debug.Log(string.Format("Took screenshot to: {0}", filename));
			}
			screenShot.Apply();
			if(onPictureTaken != null){
				onPictureTaken();
			}
		}
		catch(Exception e){
			Debug.LogError("Error saving: " + e.Message);
		}
	}
	
	public void stopCamera(){
		if (screenshotCamera.targetTexture != null) {
			//screenshotCamera.targetTexture.Release ();
			screenshotCamera.targetTexture = null;
		}
		screenshotCamera.enabled = false;
	}
	
}

