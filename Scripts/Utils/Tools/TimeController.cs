﻿using UnityEngine;
using System.Collections;

public class TimeController : MonoBehaviour {
	public static int FPS = -1;//-1 will use the true elapsed frame time
	public static float deltaTime{
		get{
			if(FPS < 0){
				return Time.deltaTime;
			}
			return 1f/FPS;
		}
	}

}
