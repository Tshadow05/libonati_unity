﻿using UnityEngine;
using System.Collections;

namespace Wild {
	//Draw a bounding box around your grabbable object
	public class BoundingBox : MonoBehaviour {
		public Vector3 startSize = Vector3.one;

		[Header("Structure")]
		public Transform[] corners; //Topleft -> Clockwise BackTopleft -> Clockwise
		public Transform volume;
		public Transform offsetHolder;

		private Vector3 size;
		private Vector3 halfSize;
		public Vector3 currentSize{
			get{
				return size;
			}
		}
		public Vector3 currentHalfSize{
			get{
				return halfSize;
			}
		}


		// Use this for initialization
		void Awake () {
			if (offsetHolder == null) {
				offsetHolder = transform;
			}
			if (startSize.x > 0 && startSize.y > 0 && startSize.z > 0) {
				updateSize (startSize);
			}
		}


		public void updateSize (Vector3 localSize){
			updateSize (localSize, Vector3.zero);
		}

		public void updateSize (Vector3 localSize, Vector3 offset){
			size = localSize;
			halfSize = localSize / 2f;
			corners [0].localPosition = new Vector3(-halfSize.x, halfSize.y, -halfSize.z); //   FRONT TOP LEFT
			corners [1].localPosition = new Vector3(halfSize.x, halfSize.y, -halfSize.z); // 	FRONT TOP RIGHT
			corners [2].localPosition = new Vector3(halfSize.x, -halfSize.y, -halfSize.z); // 	FRONT BOTTOM RIGHT
			corners [3].localPosition = new Vector3(-halfSize.x, -halfSize.y, -halfSize.z); // 	FRONT BOTTOM LEFT
			corners [4].localPosition = new Vector3(-halfSize.x, halfSize.y, halfSize.z); // 	BACK TOP LEFT
			corners [5].localPosition = new Vector3(halfSize.x, halfSize.y, halfSize.z); // 	BACK TOP RIGHT
			corners [6].localPosition = new Vector3(halfSize.x, -halfSize.y, halfSize.z); // 	BACK BOTTOM RIGHT
			corners [7].localPosition = new Vector3(-halfSize.x, -halfSize.y, halfSize.z); // 	BACK BOTTOM LEFT

			volume.localScale = localSize;
			offsetHolder.transform.localPosition = offset;
		}
	}
}