﻿using UnityEngine;

public class TempGameObject : MonoBehaviour {
    void Awake() {
        DestroyImmediate(this.gameObject);
    }
}
