﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MobileCompass : MonoBehaviour {
	public Transform compassTransform;
	public Text debugTxt;

	private float currentHeading;

	// Use this for initialization
	void Start () {
		Input.compass.enabled = true;
	}
	
	// Update is called once per frame
	void Update () {
		currentHeading = Input.compass.magneticHeading;
		if (compassTransform) {
			compassTransform.localEulerAngles = new Vector3(0,currentHeading,0);
		}
		if (debugTxt) {
			debugTxt.text = currentHeading + "°";
		}
	}

	public float getHeading(){
		return currentHeading;
	}


}
