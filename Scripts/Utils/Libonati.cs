using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System.Text.RegularExpressions;
using System.IO;

namespace Libonati {
    public class LibonatiHelper : MonoBehaviour {
		
        //GENERIC
        public enum EventState {
            AWAKE,
            START,
            UPDATE,
            FIXED_UPDATE,
            ON_ENABLE,
            ON_DISABLE,
            ON_DESTROY,
            NONE
        }

        private struct FakeTransfrom{
            public Vector3 position;
            public Vector3 scale;
            public Quaternion rotation;
            public FakeTransfrom(Vector3 position, Vector3 scale, Quaternion rotation){
                this.position = position;
                this.scale = scale;
                this.rotation = rotation;
            }
        }


        //MATH
        static public Vector3 Abs(Vector3 newVector){
            return new Vector3(Mathf.Abs(newVector.x), Mathf.Abs(newVector.y), Mathf.Abs(newVector.z));
        }

        static public int VectorCoordinateToLinear(int x, int y, int width) {
			return y * width + x;
		}

        static public Vector3 Tangent(Vector3 dir){
            Vector3 t1 = Vector3.Cross(dir, Vector3.forward );
            Vector3 t2 = Vector3.Cross(dir, Vector3.up );
            if( t1.magnitude > t2.magnitude ){
                return t1;
            }
            else{
                return t2;
            }
        }

        static public Vector2Int LinearToVectorCoordinate(int i, int width){
            Vector2Int value = new Vector2Int();
            value.x = i % width;
            value.y = (i-value.x) / width;
            return value;
        }

        static public Vector3 getRandomVector3() {
            float[,] range = new float[3, 2] { { -1, 1 }, { -1, 1 }, { -1, 1 } };
            return getRandomVector3(range);
        }

        static public Vector3 getRandomVector3(float amount) {
            return getRandomVector3() * amount;
        }

        /// <summary>
        /// "Example: float[,] range = new float[3, 2]{{-X,X},{-Y,Y},{-Z,Z}};"
        /// </summary>
        static public Vector3 getRandomVector3(float[,] range) {
            Vector3 value = new Vector3(UnityEngine.Random.Range(range[0, 0], range[0, 1]), UnityEngine.Random.Range(range[1, 0], range[1, 1]), UnityEngine.Random.Range(range[2, 0], range[2, 1]));
            return value;
        }
            //EXAMPLE RANGE
            /*
            float[,] range = new float[3, 2]{
                    {-1,1},//X
                    {-1,1},//Y
                    {-1,1} //Z
                };
            */
        public static float Round(float value, int decimalPlaces) {
            float mult = Mathf.Pow(10.0f, (float)decimalPlaces);
            return Mathf.Floor(value * mult) / mult;
        }

        public static int RoundToNextPOT(int amount){
            if (amount < 0) { return 0; }
            --amount;
            amount |= amount >> 1;
            amount |= amount >> 2;
            amount |= amount >> 4;
            amount |= amount >> 8;
            amount |= amount >> 16;
            return amount + 1;
        }
        
        public static int RoundToNearestPOT(int amount){
            int next = RoundToNextPOT(amount);
            int prev = next >> 1;
            return next - amount < amount - prev ? next : prev;
        }

        public static float clampAngle(float angle, float min, float max) {
            angle = angle % 360;
            if((angle >= -360F) && (angle <= 360F)) {
                if(angle < -360F) {
                    angle += 360F;
                }
                if(angle > 360F) {
                    angle -= 360F;
                }			
            }
            return Mathf.Clamp(angle, min, max);
        }

        //TRANSFORMATIONS
        public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles) {
            return RotatePointAroundPivot(point, pivot, Quaternion.Euler(angles));
        }

        public static Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Quaternion rotation) {
            return rotation * (point - pivot) + pivot;
        }

        private static FakeTransfrom RotateAround(Vector3 originalPosition, Quaternion originalRotation, Vector3 point, Vector3 axis, float angle) {
            FakeTransfrom t = new FakeTransfrom();
            Quaternion rot = Quaternion.AngleAxis(angle, axis); // get the desired rotation
            Vector3 dir = originalPosition - point; // find current direction relative to point
            dir = rot * dir; // rotate the direction
            t.position = point + dir; // define new position
            t.rotation = originalRotation * Quaternion.Inverse(originalRotation) * rot * originalRotation;// rotate object to keep looking at the point:
            return t;
        }

        public static bool LineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, ref Vector2 intersection) {
            float Ax, Bx, Cx, Ay, By, Cy, d, e, f, num/*,offset*/;
            float x1lo, x1hi, y1lo, y1hi;
            Ax = p2.x - p1.x;
            Bx = p3.x - p4.x;

            // X bound box test/
            if(Ax < 0) {
                x1lo = p2.x;
                x1hi = p1.x;
            } else {
                x1hi = p2.x;
                x1lo = p1.x;
            }

            if(Bx > 0) {
                if(x1hi < p4.x || p3.x < x1lo) return false;
            } else {
                if(x1hi < p3.x || p4.x < x1lo) return false;
            }

            Ay = p2.y - p1.y;
            By = p3.y - p4.y;

            // Y bound box test//
            if(Ay < 0) {  
                y1lo = p2.y;
                y1hi = p1.y;
            } else {
                y1hi = p2.y;
                y1lo = p1.y;
            }

            if(By > 0) {
                if(y1hi < p4.y || p3.y < y1lo) return false;
            } else {
                if(y1hi < p3.y || p4.y < y1lo) return false;
            }

            Cx = p1.x - p3.x;
            Cy = p1.y - p3.y;
            d = By * Cx - Bx * Cy;  // alpha numerator//
            f = Ay * Bx - Ax * By;  // both denominator//

            // alpha tests//
            if(f > 0) {
                if(d < 0 || d > f) return false;
            } else {
                if(d > 0 || d < f) return false;
            }
            e = Ax * Cy - Ay * Cx;  // beta numerator//

            // beta tests //
            if(f > 0) {   
                if(e < 0 || e > f) return false;
            } else {
                if(e > 0 || e < f) return false;
            }
            // check if they are parallel
            if(f == 0) return false;

            // compute intersection coordinates //
            num = d * Ax; // numerator //
            intersection.x = p1.x + num / f;

            num = d * Ay;
            intersection.y = p1.y + num / f;
            return true;

        }
        /*
		public static Vector3 AxisLock(Vector3 vecFrom, Vector3 vecTo, Vector3 offset)
		{     
			if (lockX || lockY || lockZ){
				if (lockX){
					vecFrom.x = vecTo.x - offset.x;
				}
				if (lockY){
					vecFrom.y = vecTo.y - offset.y;
				}
				if (lockZ){
					vecFrom.z = vecTo.z - offset.z;
				}
				return vecFrom;
			}
			else
			{
				return vecTo - offset;
			}
		}*/

        //COLLISION
        static public bool hitTestRect(Rect rect, Vector3 point) {
            return point.x >= rect.xMin && point.x <= rect.xMax && point.y <= rect.yMin && point.y >= rect.yMax;
        }

        static public bool hitTestCube(Vector3 point, Vector3 cubePosition, float cubeSize) {
            return hitTestCube(point, cubePosition, Vector3.one * cubeSize);
        }

        static public bool hitTestCube(Vector3 point, Vector3 cubePosition, Vector3 cubeSize) {
            Vector3 distance = cubePosition - point;
            cubeSize /= 2;
            return Mathf.Abs(distance.x) < cubeSize.x && Mathf.Abs(distance.y) < cubeSize.y && Mathf.Abs(distance.z) < cubeSize.z;
        }

        static public Vector2 findPositionOnCircle(Vector3 center, float radius, float angleInDegrees) {
            return new Vector2(center.x + radius * Mathf.Cos(Mathf.Deg2Rad * angleInDegrees), center.y + radius * Mathf.Sin(Mathf.Deg2Rad * angleInDegrees));
        }

        static public Bounds GetWorldBounds(MeshRenderer[] renderers) {
            Bounds newBounds = new Bounds();
            bool first = true;
            foreach(MeshRenderer ren in renderers) {
                Bounds renBounds = ren.bounds;
                if(first) {
                    newBounds = renBounds;
                } else {
                    newBounds.Encapsulate(renBounds);
                }
                first = false;
            }
            return newBounds;
        }

        static public Bounds GetLocalBounds(Transform transform) {
            bool first = true;
            Bounds localBounds = new Bounds();
            foreach (MeshRenderer ren in transform.gameObject.GetComponentsInChildren<MeshRenderer>()){
                MeshFilter filter = ren.gameObject.GetComponent<MeshFilter>();
                if(filter == null){
                    continue;
                }
                if(first){
                    localBounds.center = transform.InverseTransformPoint(filter.transform.TransformPoint(filter.mesh.bounds.center));
                    first = false;
                }
                Mesh mesh = filter.sharedMesh != null ? filter.sharedMesh : filter.mesh;
                if(mesh != null){
                    Libonati.LibonatiHelper.EncapsulateFromOtherAxis(ref localBounds, transform, ren.transform, mesh.bounds);
                }
            }
            return localBounds;
        }

        static public void EncapsulateFromOtherAxis(ref Bounds myBounds, Transform myT, Transform incomingT,  Bounds incomingBounds){
            foreach(Vector3 worldPoint in incomingBounds.Corners()){
                //Convert each corner into myT coordinate space and stretch myBounds to contain it
                myBounds.Encapsulate(myT.InverseTransformPoint(incomingT.TransformPoint(worldPoint)));
            }
        }

        //System / IO
        static public void SaveTextureToFile(Texture2D texture, string fullPath){
            byte[] bytes= texture.EncodeToPNG();
            FileStream file = File.Open(fullPath, FileMode.Create);
            BinaryWriter binary= new BinaryWriter(file);
            binary.Write(bytes);
            file.Close();
            binary.Close();
        }

        //SCENE MANAGMENT
        static public void reloadScene() {
            //Depricated
            //Application.LoadLevel (Application.loadedLevel);
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        static public void loadNextScene() {
            //Depricated
            //int levelId = Application.loadedLevel;
            int levelId = SceneManager.GetActiveScene().buildIndex;
            levelId++;
            //Depricated
            //if(levelId >= Application.levelCount){
            if(levelId >= SceneManager.sceneCountInBuildSettings) {
                levelId = 0;
            }
            //Depricated
            //Application.LoadLevel(levelId);
            SceneManager.LoadScene(levelId, LoadSceneMode.Additive);
        }

        static public void loadPrevScene() {
            //Depricated
            //int levelId = Application.loadedLevel;
            int levelId = SceneManager.GetActiveScene().buildIndex;
            levelId--;
            if(levelId < 0) {
                //Depricated
                //levelId = Application.levelCount-1;
                levelId = SceneManager.sceneCount - 1;
            }
            //Depricated
            //Application.LoadLevel(levelId);
            SceneManager.LoadScene(levelId);
        }

        public static float ConvertRange(float originalStart, float originalEnd, float newStart, float newEnd, float value) {
            float scale = (float)(newEnd - newStart) / (originalEnd - originalStart);
            return (newStart + ((value - originalStart) * scale));
        }

        //Text Modification
        static public string padInt(int value, int numberOfDigits) {
            return value.ToString("D" + numberOfDigits.ToString());
        }

        static public string clockString(int totalSeconds) {		
            int minutes = (int)Mathf.Floor(totalSeconds / 60);
            int seconds = (int)Mathf.Floor(totalSeconds - minutes * 60);
            string clock = minutes + ":" + LibonatiHelper.padInt(seconds, 2);
            return clock;
        }

        static public char[] generateTextCode(int codeLength) {
            return generateCode(codeLength, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
        }

        static public char[] generateNumberCode(int codeLength) {
            return generateCode(codeLength, "1234567890");
        }

        static public char[] generateCode(int codeLength, string possibleCharacters) {
            char[] codeChar = new char[codeLength];
            for(int i = 0; i < codeLength; i++) {
                codeChar[i] = possibleCharacters[(int)UnityEngine.Random.Range(0, possibleCharacters.Length)];
            }
            return codeChar;
        }

        //GameObject Modification    
        public static bool IsReallyNull(System.Object obj) { return System.Object.ReferenceEquals(obj, null); }

        static public void destroyAllChildren(GameObject parent) {
            foreach(Transform child in parent.transform) Destroy(child.gameObject);
        }

        public static Vector2 getPositionFromLinearArray(int i, int width) {
            return new Vector2(i % width, Mathf.Floor(i / width));
        }

        static public void showAllSprites(GameObject obj) {
            toggleAllSpriteVisibility(obj, true);
        }

        static public void hideAllSprites(GameObject obj) {
            toggleAllSpriteVisibility(obj, false);
        }

        static public void toggleAllSpriteVisibility(GameObject obj, bool show) {
            foreach(SpriteRenderer ren in obj.GetComponentsInChildren<SpriteRenderer>()) {
                ren.enabled = show;
            }
        }

        static public void SetLayerRecursive(Transform t, int layer) {
            t.gameObject.layer = layer; 
            for(int i = 0; i < t.childCount; i++) {
                SetLayerRecursive(t.GetChild(i), layer);
            }
        }

        static public void resetTransform(Transform t) {
            t.localPosition = Vector3.zero;
            t.localRotation = Quaternion.identity;
            t.localScale = Vector3.one;
        }

        static public void showAllMesh(GameObject obj) {
            toggleAllMeshVisibility(obj, true);
        }

        static public void hideAllMesh(GameObject obj) {
            toggleAllMeshVisibility(obj, false);
        }

        static public void toggleAllMeshVisibility(GameObject obj, bool show) {
            foreach(MeshRenderer ren in obj.GetComponentsInChildren<MeshRenderer>()) {
                ren.enabled = show;
            }
        }

        static public void hideAll(GameObject obj) {
            if(obj != null) {
                hideAllMesh(obj);
                hideAllSprites(obj);
            }	
        }

        static public void showAll(GameObject obj) {
            if(obj != null) {
                showAllMesh(obj);
                showAllSprites(obj);
            }
        }

        //SOMETIMES Unity will leave the rigidibody asleep not knowing the collision has updated or something
        //The result is that you can grab something until the object moves.
        //HACK: this forces the rigidboy to reprocess itself
        static public IEnumerator ForceCollisionRecalculate(Rigidbody rigid) {
            rigid.isKinematic = false;
            yield return null;//Wait 1 frame
            rigid.isKinematic = true;
        }

        static public IEnumerator ForceCollisionRecalculate(BoxCollider col) {
            col.enabled = false;
            yield return null;//Wait 1 frame
            col.enabled = true;
        }


        static public object getRandomEnumValue(Type enumType) {
            Array values = Enum.GetValues(enumType);
            return values.GetValue((int)UnityEngine.Random.Range(0, values.Length));
        }

        static public void disable2DColliders(GameObject obj) {
            foreach(Collider2D col in obj.GetComponentsInChildren<Collider2D>()) {
                col.enabled = false;
            }
        }

        static public void enable2DColliders(GameObject obj) {
            foreach(Collider2D col in obj.GetComponentsInChildren<Collider2D>()) {
                col.enabled = true;
            }
        }

        static public void disable3DColliders(GameObject obj) {
            foreach(Collider col in obj.GetComponentsInChildren<Collider>()) {
                col.enabled = false;
            }
        }

        static public void enable3DColliders(GameObject obj) {
            foreach(Collider col in obj.GetComponentsInChildren<Collider>()) {
                col.enabled = true;
            }
        }

        static public void disableColliders(GameObject obj) {
            disable2DColliders(obj);
            disable3DColliders(obj);
        }

        static public void enableColliders(GameObject obj) {
            enable2DColliders(obj);
            enable3DColliders(obj);
        }


        //Grid Based
        public static int tileToLinear(int x, int y, int stride) {
            return y * stride + x;
        }

        public static Vector2 linearToTile(int i, int stride) {
            return new Vector2(i % stride, Mathf.Floor(i / stride));
        }


        //Color Manipulation
        public static string colorToString(Color color) {
            return color.r + "," + color.g + "," + color.b + "," + color.a; 
        }

        public static Color stringToColor(string colorString) {
            try {
                string[] colors = colorString.Split(',');
                return new Color(float.Parse(colors[0]), float.Parse(colors[1]), float.Parse(colors[2]), float.Parse(colors[3]));
            } catch {
                return Color.white;
            }
        }

        public static Color getRandomColor() {
            return new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), 1);
        }

        // Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
        public static string colorToHex(Color32 color) {
            string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
            return hex;
        }

        public static Color hexToColor(string hex) {
            hex = hex.Replace("0x", "");//in case the string is formatted 0xFFFFFF
            hex = hex.Replace("#", "");//in case the string is formatted #FFFFFF
            byte a = 255;//assume fully visible unless specified in hex
            byte r = byte.Parse(hex.Substring(0, 2), System.Globalization.NumberStyles.HexNumber);
            byte g = byte.Parse(hex.Substring(2, 2), System.Globalization.NumberStyles.HexNumber);
            byte b = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            //Only use alpha if the string has enough characters
            if(hex.Length == 8) {
                a = byte.Parse(hex.Substring(4, 2), System.Globalization.NumberStyles.HexNumber);
            }
            return new Color32(r, g, b, a);
        }

        public static Color brightenColor(Color originalColor, float brightenAmount = 1) {
            return new Color(Mathf.Min(1, originalColor.r + brightenAmount), Mathf.Min(1, originalColor.g + brightenAmount), Mathf.Min(1, originalColor.b + brightenAmount), originalColor.a);
        }
    }

    [System.Serializable]
    public class Vector2i {
        [SerializeField]
        protected int _x = 0;
        [SerializeField]
        protected int _y = 0;

        [SerializeField]
        public int x {
            get {
                return _x;
            }
            set {
                _x = value;
            }
        }

        public int y {
            get {
                return _y;
            }
            set {
                _y = value;
            }
        }

        public Vector2i() {
            this.x = 0;
            this.y = 0;
        }

        public Vector2i(int value) {
            this.x = this.y = value;
        }

        public Vector2i(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Vector2i(Vector2 latLon) {
            this.x = (int)latLon.x;
            this.y = (int)latLon.y;
        }

        private Vector2 vec2 {
            get {
                return new Vector2(_x, _y);
            }
        }

        public override string ToString() {
            return _x + ", " + _y;
        }

        public override int GetHashCode() {
            return x.GetHashCode() + y.GetHashCode();
        }

        public override bool Equals(object obj) {
            Vector2i other = obj as Vector2i;
            return other.x == _x && other.y == _y;
        }

        public static Vector2i operator +(Vector2i c1, Vector2i c2){
            return new Vector2i(c1.x + c2.x, c1.y + c2.y);
        }
    }

    public class Vector3i : Vector2i {
        [SerializeField]
        private int _z = 0;

        public int z {
            get {
                return _z;
            }
            set {
                _z = value;
            }
        }

        public Vector3i() : base() {
            this.z = 0;
        }

        public Vector3i(int value) : base(value) {
            this.z = value;
        }

        public Vector3i(int x, int y, int z) : base(x, y) {
            this.z = z;
        }

        public Vector3i(Vector3 pos) : base((int)pos.x, (int)pos.y) {
            this.z = (int)pos.z;
        }

        public Vector3 vec3 {
            get {
                return new Vector3(_x, _y, _z);
            }
        }

        public override string ToString() {
            return _x + ", " + _y + ", " + _z;
        }

        public override int GetHashCode() {
            return x.GetHashCode() + y.GetHashCode() + z.GetHashCode();
        }

        public override bool Equals(object obj) {
            Vector3i other = obj as Vector3i;
            return other.x == _x && other.y == _y && other.z == _z;
        }

        public static Vector3i operator +(Vector3i c1, Vector3i c2){
            return new Vector3i(c1.x + c2.x, c1.y + c2.y, c1.z + c2.z);
        }

        public static Vector2i operator +(Vector2i c1, Vector3i c2){
            return new Vector2i(c1.x + c2.x, c1.y + c2.y);
        }

        public static Vector3i operator +(Vector3i c1, Vector2i c2){
            return new Vector3i(c1.x + c2.x, c1.y + c2.y, c1.z);
        }
    }
    //DATA MANAGEMENT

    public class DictionaryComparer<K, V> {

        public static Dictionary<K, V> findCommonElements(Dictionary<K, V> A, Dictionary<K, V> B) {
            Dictionary<K, V> result = new Dictionary<K, V>();
            foreach(KeyValuePair<K, V> kvp in B) {
                try {
                    if(A.ContainsKey(kvp.Key)) {
                        result.Add(kvp.Key, kvp.Value);
                    }
                } catch(Exception e) {
                    Debug.LogError(e.Message);
                }
            }
            return result;
        }

        public static Dictionary<K, V> subtractElements(Dictionary<K, V> A, Dictionary<K, V> B) {
            foreach(KeyValuePair<K, V> kvp in B) {
                try {
                    if(A.ContainsKey(kvp.Key)) {
                        A.Remove(kvp.Key);
                    }
                } catch(Exception e) {
                    Debug.LogError(e.Message);
                }
            }
            return A;
        }
    }

}

//EXTENSION
public static class LibonatiExtensions {

    //TRANSFORM
    static public Transform ResetTransform(this Transform t) {
        t.localPosition = Vector3.zero;
        t.localRotation = Quaternion.identity;
        t.localScale = Vector3.one;
        return t;
    }

        /// <summary>
        /// Transforms rotation from local space to world space
        /// </summary>
    public static Quaternion TransformRotation(this Transform t, Quaternion rotation) {
        return t.rotation * rotation;
    }

        /// <summary>
        /// Transforms rotation from world space to local space
        /// </summary>
    public static Quaternion InverseTransformRotation(this Transform t, Quaternion rotation) {
        return Quaternion.Inverse(t.rotation) * rotation;
    }

    public static void SetLocalTransform(this Transform t, Vector3 newLocalPosition, Quaternion newLocalRotation, Vector3 newScale){
        t.localPosition = newLocalPosition;
        t.localRotation = newLocalRotation;
        t.localScale = newScale;
    }

    public static void SetLayerRecursively(this Transform transform, LayerMask layer){
        transform.gameObject.layer = layer;
        foreach(Transform t in transform){
            t.SetLayerRecursively(layer);
        }
    }

    public static void SetLayerRecursively(this GameObject go, LayerMask layer){
        go.transform.SetLayerRecursively(layer);
    }

    //VECTOR
    public static Vector3 MultiplyBy(this Vector3 v, Vector3 other) {
        return new Vector3(v.x * other.x, v.y * other.y, v.z * other.z);
    }
    
    //FLOAT
    public static bool IsAboutEqual(this float f, float compareValue, float margin) {
        return f > compareValue - margin && f < compareValue + margin;
    }

    //STRING
    public static string ToUpperFirstOfEachWord(this string s){
        return Regex.Replace(s, @"(^\w)|(\s\w)", m => m.Value.ToUpper());
    }

    //LISTS
    private static System.Random rng = new System.Random();
    public static void Shuffle<T>(this IList<T> list) {  
        int n = list.Count;  
        while(n > 1) {  
            n--;  
            int k = rng.Next(n + 1);  
            T value = list[k];  
            list[k] = list[n];  
            list[n] = value;  
        }  
    }

    //BOUNDS
    static public Vector3[] Corners(this Bounds bounds){
        float length = bounds.extents.x;
        float height = bounds.extents.y;
        float depth = bounds.extents.z;
        return new Vector3[8]{
            bounds.center + new Vector3( -length,    -height, depth ),
            bounds.center + new Vector3( length,     -height, depth ),
            bounds.center + new Vector3( length,     -height, -depth ),
            bounds.center + new Vector3( -length,    -height, -depth ),  
            bounds.center + new Vector3( -length,    height,  depth ),
            bounds.center + new Vector3( length,     height,  depth ),
            bounds.center + new Vector3( length,     height,  -depth ),
            bounds.center + new Vector3( -length,    height,  -depth )
        };
    }

    //TEXTURES
     public static bool HasAlpha(this Texture2D texture){
        Color[] aColors = texture.GetPixels();
        for(int i = 0; i < aColors.Length; i++)
            if (aColors[i].a < 1f)
                return true;
        return false;
    }

    public static Sprite CreateSprite(this Texture2D texture){
         return Sprite.Create(texture, new Rect(0,0,texture.width,texture.height), new Vector2(.5f, .5f), texture.width);
    }

    // MATRIX
    public static Matrix4x4 AddMatrix(this Matrix4x4 a, Matrix4x4 b)
    {
        return new Matrix4x4(
            a.GetColumn(0) + b.GetColumn(0),
            a.GetColumn(1) + b.GetColumn(1),
            a.GetColumn(2) + b.GetColumn(2),
            a.GetColumn(3) + b.GetColumn(3)
            );
    }

    public static Color Composite( this Color under, Color over) {
        float invAlpha = (1.0f - over.a);
        return new Color(under.r * invAlpha + over.r, under.g * invAlpha + over.g, under.b * invAlpha + over.b, under.a * invAlpha + over.a);
    }
}