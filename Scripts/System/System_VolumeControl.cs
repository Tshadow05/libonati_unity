﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class System_VolumeControl : MonoBehaviour{
	private const int VOLUME_COMMAND = 0x319;
	private const int UP_COMMAND = 0xA0000;
	private const int DOWN_COMMAND = 0x90000;

	//PUBLIC VARIABLES
	public KeyCode increaseBtn = KeyCode.KeypadPlus;
	public KeyCode decreaseBtn = KeyCode.KeypadMinus;

	//SYSTEM FUNCTIONS
	[DllImport("user32.dll")]
	private static extern IntPtr GetActiveWindow ();

	//for volume up and down commands
	[DllImport("user32.dll")]
	private static extern IntPtr SendMessageW (IntPtr hWnd, int Msg, IntPtr wParam, IntPtr lParam);

	//for direct volume control
	[DllImport("winmm.dll")]
	private static extern int waveOutGetVolume (IntPtr hwo, out uint dwVolume);
	[DllImport("winmm.dll")]
	private static extern int waveOutSetVolume (IntPtr hwo, uint dwVolume);

	//vista 7+ volume support
	[Guid("87CE5498-68D6-44E5-9215-6DA47EF883D8"), InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	internal interface ISimpleAudioVolume
	{
		[PreserveSig]
		int SetMasterVolume(float fLevel, ref Guid EventContext);

		[PreserveSig]
		int GetMasterVolume(out float pfLevel);

		[PreserveSig]
		int SetMute(bool bMute, ref Guid EventContext);

		[PreserveSig]
		int GetMute(out bool pbMute);
	}

	public IntPtr windowHandle {
		get {
			return GetActiveWindow ();
		}
	}

	public float volume {
		get {
			//THIS ONLY APPLYS TO THE APPLICATION AUDIO NOT THE SYSTEM!
			uint v = 0;
			waveOutGetVolume(windowHandle, out v);
			ushort V = (ushort)(v & 0x0000ffff);
			return (float)V / (float)ushort.MaxValue;
		}
		set {
			setVolume (value);
		}
	}


	// Use this for initialization
	void Awake () {
	}

	protected virtual void Update(){
		if (Input.GetKey(increaseBtn)) {
			increaseVolume ();
		} else if (Input.GetKey (decreaseBtn)) {
			decreaseVolume ();
		}
	}

	public void setVolume(float newVolume){
		
	}

	public void increaseVolume(){
		SendMessageW (windowHandle,VOLUME_COMMAND,windowHandle,(IntPtr)UP_COMMAND);
		logVolume ();
	}
	public void decreaseVolume(){
		SendMessageW (windowHandle,VOLUME_COMMAND,windowHandle,(IntPtr)DOWN_COMMAND);
		logVolume ();
	}

	public void logVolume(){
		Debug.Log ("Current System Volume: " + volume);
	}

}
