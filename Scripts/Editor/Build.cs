﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.Build.Reporting;

namespace Wild {
	public static class Build {
		static void DoARiOSBuild( bool exitOnError ) {
			Debug.Log("Building AR viewer for iOS");
			BuildPlayerOptions options = new BuildPlayerOptions();
			options.scenes = new[] { "Assets/WILD/_Wildspace/Scenes/MainMobileAR.unity" };
			options.locationPathName = "Builds/ARiOS";
			options.target = BuildTarget.iOS;
			options.options = BuildOptions.SymlinkLibraries;
			BuildReport report = BuildPipeline.BuildPlayer(options);

			Debug.Log("WILD AR iOS Build Report");
			Debug.Log("------------------------");

			bool anyErrors = false;
			foreach (var step in report.steps) {
				foreach (var msg in step.messages) {
					Debug.unityLogger.Log(msg.type, msg.content);
					anyErrors |= msg.type == LogType.Error;
				}
			}

			if (exitOnError && anyErrors)
				EditorApplication.Exit(-1);
		}

		[MenuItem("Wild/Build/AR iOS")]
		static void BuildARiOSMenuItem() {
			DoARiOSBuild(false);
		}

		static void BuildARiOS() {
			Wild.BuildSupport.PreExport();
			DoARiOSBuild(true);
		}
	}
}
