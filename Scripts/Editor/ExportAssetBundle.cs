﻿using UnityEditor;
using UnityEngine;
using System;
using System.IO;

public class CreateAssetBundles
{
    [MenuItem("Assets/Build AssetBundles")]
    static void BuildAllAssetBundles()
    {
		string path = Application.dataPath + "/StreamingAssets";
		//Debug.Log("Path: " + path);
		if(!Directory.Exists(path)){
			Directory.CreateDirectory (path);
		}
		path = "Assets/StreamingAssets";
		BuildPipeline.BuildAssetBundles(path, BuildAssetBundleOptions.UncompressedAssetBundle, BuildTarget.StandaloneWindows64);
    }
}