﻿using UnityEngine;
using UnityEditor;
using System.IO;

public class DestroyOnSceneSave : UnityEditor.AssetModificationProcessor {

    static string[] OnWillSaveAssets(string[] paths) {
        string sceneName = string.Empty;

        foreach (string path in paths) {
            if (path.Contains(".unity")) {
                sceneName = Path.GetFileNameWithoutExtension(path);

                UnityEngine.SceneManagement.Scene scene = UnityEngine.SceneManagement.SceneManager.GetSceneByName(sceneName);

                GameObject[] rootObjects = scene.GetRootGameObjects();

                foreach(GameObject obj in rootObjects) {
                    if(obj.GetComponent<TempGameObject>() != null){
                        while(obj.transform.childCount > 0){
                            GameObject.DestroyImmediate(obj.transform.GetChild(0).gameObject);
                        }
                    }
                }
            }
        }

        return paths;
    }
}
