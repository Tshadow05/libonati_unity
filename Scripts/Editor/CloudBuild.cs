﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

namespace Wild {

public class BuildSupport : MonoBehaviour {
    // Location of the app that runs the web request.
//    private static string GurlPath = @"tooling\gurl.exe";
    private static string GurlPath = @"curl";

//    public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject obj) {
    public static void PreExport() {
		GenerateVersionFile();
	}

	/// <summary>
	/// Create a file that contains my version number, for the build system.
	/// </summary>
    private static void GenerateVersionFile() {
		string version = string.Empty;
		// Nothing seems to trigger the tag being there.
//		version = GetTag();
		version = GetAppVersion();
		if (!string.IsNullOrEmpty(version)) {
			try {
				string dst_path = Path.Combine("Assets", "StreamingAssets");
				dst_path = Path.Combine(dst_path, "version.txt");
				string[] lines = { "{\"version\": \"" + version + "\"}" };
				File.WriteAllLines(dst_path, lines);
			} catch (System.Exception ex) {
				Debug.Log("CloudBuid.PreExport() error writing version file: " + ex.Message);
			}
		}
	}
    
	private static string GetTag() {
        var commit = BuildManifest.GetCommit();
        if (string.IsNullOrEmpty(commit)) {
            Debug.Log("CloudBuid.PreExport() error: no commit");
			return null;
        }
		var tag = Bitbucket.GetTagForCommit(commit);
		if (string.IsNullOrEmpty(tag)) {
			Debug.Log("CloudBuid.PreExport() error: no tag");
			return null;
		}
		return tag;
	}

	private static string GetAppVersion() {
		return Application.version;
	}

    /// <summary>
    /// Wrap the build manifest.
    /// </summary>
    public class BuildManifest  {
    
        public static string GetCommit() {
            try {
                var fn = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "build_manifest.json");
                var n = SimpleJSON.JSONNode.Parse(File.ReadAllText(fn));
                return n["scmCommitId"];
            } catch (System.Exception ex) {
                Debug.Log("CloudBuid.BuildManifest.GetCommit() error: " + ex.Message);
            }
            return null;
        }
    }

    /// <summary>
    /// Wrap the bitbucket API.
    /// </summary>
    public class Bitbucket  {
        
        public static string GetTagForCommit(string commit) {
            if (string.IsNullOrEmpty(commit)) return null;
            var token = GetToken();
            if (!string.IsNullOrEmpty(token)) {
                return GetTagForCommit(token, commit);
            }
            return null;
        }
 
        private static string GetToken() {
            var req = new Request(Request.Method.Post, @"https://bitbucket.org/site/oauth2/access_token");
            req.AddFormData("grant_type", "client_credentials");
            req.AddFormData("client_id", "9vVUNdVNyf4vKNGwnt");
            req.AddFormData("client_secret", "39s2JJtrFXp2D8mDC3752cjEyXM9YFmc");
            try {
                var n = SimpleJSON.JSON.Parse(req.Run());
                if (n == null) return null;
                return n["access_token"];
            } catch (System.Exception ex) {
                Debug.Log("CloudBuid.Bitbucket.GetToken() error: " + ex.Message);
            }
            return null;
        }

        private static string GetTagForCommit(string bearerToken, string commit) {
            // I don't see any way in the API to look up a specific commit, so I have to iterate the pages.
            var req = new Request(Request.Method.Get, @"https://bitbucket.org/api/2.0/repositories/builtbywild/ws_vr_unity/refs/tags");
            req.AddHeader("Authorization", "Bearer " + bearerToken);
            req.AddHeader("Cache-Control", "no-cache");
            while (!string.IsNullOrEmpty(req.url)) {
                try {
                    var res = req.Run();
                    req.url = string.Empty;
                    var n = SimpleJSON.JSONNode.Parse(res);
                    var sub = n["next"];
                    if (sub != null) req.url = sub;

                    if ((sub = n["values"]) == null) continue;
                    var a = sub as SimpleJSON.JSONArray;
                    if (a == null) continue;

    			    foreach (SimpleJSON.JSONNode an in a) {
                        if ((sub = an["name"]) == null) continue;
                        string name = sub;
                        if ((sub = an["target"]) == null) continue;
                        var obj = sub as SimpleJSON.JSONClass;
                        if (obj == null) continue;
                        if ((sub = obj["hash"]) == null) continue;
                        if (sub.Value == commit) return name;
                    }
                } catch (System.Exception ex) {
                    Debug.Log("CloudBuid.Bitbucket.GetTagForCommit() error: " + ex.Message);
                }
            }
            return null;
        }
    }

    /// <summary>
    /// A web request.
    /// </summary>
    private class Request {
        public enum Method { Get, Post }
        public Method method = Method.Get;
        public string url { get; set; }
        private Dictionary<string, string> _headers = new Dictionary<string, string>();
        private Dictionary<string, string> _formData = new Dictionary<string, string>();

        public Request(Method m, string url) {
            method = m;
            this.url = url;
        }

        public void AddHeader(string key, string value) {
            _headers[key] = value;
        }

        public void AddFormData(string key, string value) {
            _formData[key] = value;
        }

        public string Run() {
            var process = new System.Diagnostics.Process();
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.FileName = GurlPath;
            process.StartInfo.CreateNoWindow = true;
            var sb = new System.Text.StringBuilder();
            sb.Append("-X ");
            if (method == Request.Method.Get) sb.Append("GET");
            else if (method == Request.Method.Post) sb.Append("POST");
            sb.Append(" \"");
            sb.Append(url);
            sb.Append("\"");
            foreach (var kv in _headers) {
                sb.Append(" -H \"");
                sb.Append(kv.Key);
                sb.Append(": ");
                sb.Append(kv.Value);
                sb.Append("\"");
            }
            foreach (var kv in _formData) {
                sb.Append(" -d ");
                sb.Append(kv.Key);
                sb.Append("=");
                sb.Append(kv.Value);
            }

            process.StartInfo.Arguments = sb.ToString();
            process.Start();
            string body = string.Empty;
            while (!process.StandardOutput.EndOfStream) {
                var line = process.StandardOutput.ReadLine();
                body = body + line;
            }
            return body;
        }
    }
}

}
