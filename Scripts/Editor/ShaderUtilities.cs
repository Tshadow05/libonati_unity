﻿using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;
using UnityEditor.Rendering;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;

namespace Wild {
	public class ShaderUtilitiesBuildPreprocessor : IPreprocessBuildWithReport {
		public int callbackOrder { get { return 0; } }

		public void OnPreprocessBuild(BuildReport report) {
			ShaderTrackingPreprocessor.BeginTracking();
		}
	}

	public class ShaderUtilitiesBuildPostprocessor : IPostprocessBuildWithReport {
		public int callbackOrder { get { return 0; } }
		public void OnPostprocessBuild(BuildReport report) {
			ShaderTrackingPreprocessor.EndTracking();
		}
	}

	public class ShaderTrackingPreprocessor : IPreprocessShaders {
		class TrackedPassInfo {
			public Dictionary<ShaderType, int> shaderTypeVariants = new Dictionary<ShaderType, int>();
		}
		class TrackedShaderInfo {
			public Dictionary<PassType, TrackedPassInfo> passInfo = new Dictionary<PassType, TrackedPassInfo>();
		};
		public int callbackOrder { get { return 1; } }
		public void OnProcessShader(Shader shader, ShaderSnippetData snippet, IList<ShaderCompilerData> data) {
			if (shaderTrackingLog == null)
				return;

			lock (shaderTrackingLog) {
				TrackedShaderInfo shaderInfo;
				if ( !trackedShaderInfo.TryGetValue( shader, out shaderInfo ) ) {
					shaderInfo = new TrackedShaderInfo();
					trackedShaderInfo[shader] = shaderInfo;
				}
				TrackedPassInfo passInfo;
				if ( !shaderInfo.passInfo.TryGetValue( snippet.passType, out passInfo )) {
					passInfo = new TrackedPassInfo();
					shaderInfo.passInfo[snippet.passType] = passInfo;
				}
				int variantCount;
				if ( !passInfo.shaderTypeVariants.TryGetValue( snippet.shaderType, out variantCount )) {
					variantCount = 0;
				}
				passInfo.shaderTypeVariants[snippet.shaderType] = variantCount + data.Count;

				shaderTrackingLog.WriteLine("Shader : " + shader.name);
				shaderTrackingLog.WriteLine("    Snippet : " + snippet.passType.ToString() + " \"" + snippet.passName + "\" " + snippet.shaderType.ToString());
				foreach (var d in data)
				{
					shaderTrackingLog.WriteLine("        Variant : " + d.shaderCompilerPlatform.ToString() + " - " + d.graphicsTier.ToString() + " - " + string.Join(" | ", ShaderUtilities.FlagEnumerator(d.shaderRequirements).Select((x) => (x.ToString()))));
					shaderTrackingLog.WriteLine("            Platform Keywords : " + string.Join(", ", d.platformKeywordSet.EnabledBuiltinDefines().Select((define) => define.ToString())));
					shaderTrackingLog.WriteLine("            Shader Keywords : " + string.Join(", ", d.shaderKeywordSet.GetShaderKeywords().Select((ShaderKeyword k) => { return k.GetName(); })));
				}
			}
		}

		static StreamWriter shaderTrackingLog;
		static Dictionary<Shader, TrackedShaderInfo> trackedShaderInfo = new Dictionary<Shader, TrackedShaderInfo>();
		public static void BeginTracking() {
			shaderTrackingLog = new StreamWriter( "shaderTracking.log" );
		}

		public static void EndTracking() {
			shaderTrackingLog.WriteLine("Summary : ");
			foreach ( var trackedShaderKvp in trackedShaderInfo ) {
				shaderTrackingLog.WriteLine("    " + trackedShaderKvp.Key.name);
				foreach (var trackedPassTypeKvp in trackedShaderKvp.Value.passInfo) {
					shaderTrackingLog.WriteLine("        " + trackedPassTypeKvp.Key.ToString());
					foreach (var trackedShaderTypeKvp in trackedPassTypeKvp.Value.shaderTypeVariants) {
						shaderTrackingLog.WriteLine("            " + trackedShaderTypeKvp.Key.ToString() + " : " + trackedShaderTypeKvp.Value);
					}
				}
			}

			shaderTrackingLog.Close();
			shaderTrackingLog = null;
		}
	}

	public class ShaderBuildPreprocessor : IPreprocessShaders {
		public static ShaderKeyword directionalCookieKeyword = new ShaderKeyword("DIRECTIONAL_COOKIE");
		public static ShaderKeyword[] ignoreFwdBaseKeywords = new ShaderKeyword[] {
			directionalCookieKeyword,
		};
		public static ShaderKeyword[] ignoreFwdAddKeywords = new ShaderKeyword[] {
			new ShaderKeyword("SPOT"),
			new ShaderKeyword("SPOT_COOKIE"),
			new ShaderKeyword("POINT_COOKIE"),
			directionalCookieKeyword,
		};


		public int callbackOrder { get { return 0; } }
		public void OnProcessShader(Shader shader, ShaderSnippetData snippet, IList<ShaderCompilerData> data) {
			//int origCount = data.Count;
			switch (snippet.passType) {
				case PassType.ForwardBase:
					for (int i = 0; i < data.Count; i++) {
						ShaderKeywordSet keywordSet = data[i].shaderKeywordSet;
						if (ignoreFwdBaseKeywords.Any(keywordSet.IsEnabled)) {
							data.RemoveAt(i--);
						}
					}
					break;
				case PassType.ForwardAdd:
					for (int i = 0; i < data.Count; i++) {
						ShaderKeywordSet keywordSet = data[i].shaderKeywordSet;
						if (ignoreFwdAddKeywords.Any(keywordSet.IsEnabled)) {
							data.RemoveAt(i--);
						}
					}
					break;
				case PassType.Deferred:
					break;
				case PassType.Meta:
					data.Clear();
					break;
			}
			//Debug.Log("Stripped " + (origCount - data.Count) + " variations.");
		}
	}
	public static class ShaderUtilities {
		public static IEnumerable<T> FlagEnumerator<T>(T flags) where T:IConvertible {
			UInt64 flagVal = Convert.ToUInt64(flags);
			return Enum.GetValues(typeof(T)).Cast<T>().Where((x) => (Convert.ToUInt64(x) & flagVal) != 0);
		}

		public static IEnumerable<BuiltinShaderDefine> EnabledBuiltinDefines(this PlatformKeywordSet keywordSet) {
			return Enum.GetValues(typeof(BuiltinShaderDefine)).Cast<BuiltinShaderDefine>().Where(keywordSet.IsEnabled);
		}
		static IEnumerable<T> Prepend<T>(this IEnumerable<T> source, T element) {
			yield return element;
			foreach (var x in source) {
				yield return x;
			}
		}

		// Iterate sequences that consist of one element from each IEnumerable<T> in items
		static IEnumerable<IEnumerable<T>> Choices<T>( this IEnumerable<IEnumerable<T>> items) {
			IEnumerable<T> first = items.FirstOrDefault();
			IEnumerable<IEnumerable<T>> rest = items.Skip(1);
			if (rest.Any()) {
				rest = Choices(rest);
				foreach (var a in first) {
					foreach (var b in rest) {
						yield return b.Prepend(a);
					}
				}
			} else if (first != null) {
				foreach (var a in first) {
					yield return new T[] { a };
				}
			}
		}

		static IEnumerable<IEnumerable<T>> SimpleJoin<T>(this IEnumerable<IEnumerable<T>> a, IEnumerable<IEnumerable<T>> b) {
			foreach (var x in a) {
				foreach (var y in b) {
					yield return x.Concat(y);
				}
			}
		}

		static Func<A, C> Compose<A, B, C>(Func<B, C> second, Func<A, B> first) {
			return (A a) => { return second(first(a)); };
		}

		static bool Not(bool x) { return !x; }

		public static IEnumerable<ShaderVariantCollection.ShaderVariant> EnumShaderPassVariants(Shader shader, PassType passType, IEnumerable<IEnumerable<string>> keywordSets ) {
			foreach (var keywordSet in keywordSets) {
				ShaderVariantCollection.ShaderVariant variant = new ShaderVariantCollection.ShaderVariant();
				var nonEmptyKeywords = keywordSet.Where((s)=>!String.IsNullOrEmpty(s));
				bool success;
				try {
					string[] keywordArray = nonEmptyKeywords.ToArray();
					variant = new ShaderVariantCollection.ShaderVariant(shader, passType, keywordArray );
					success = true;
				} catch (ArgumentException) {
					Debug.LogWarning("Ignoring permutation { " + passType.ToString() + " : " + string.Join(", ", nonEmptyKeywords) + " }");
					success = false;
				}
				if (success)
					yield return variant;
			}
		}

		// Some of this is just here for reference
		/*
		// NOTE [AM 8/13/2018] *_cookie features are analogous as of 2018.2.2f1
		// Relevant if DIRECTIONAL is enabled
		static string[][] directionalShadowFeatures = new string[][] {
				new string[] { "", "SHADOWS_SCREEN" },
				new string[] { "", "LIGHTMAP_SHADOW_MIXING" },
				new string[] { "", "SHADOWS_SHADOWMASK" },
			};

		// Relevant if SPOT is enabled
		static string[][] spotShadowFeatures = new string[][] {
				new string[] { "", "SHADOWS_DEPTH" },
				new string[] { "", "SHADOWS_SOFT" },
				new string[] { "", "LIGHTMAP_SHADOW_MIXING" },
				new string[] { "", "SHADOWS_SHADOWMASK" },
			};

		// Relevant if POINT is enabled
		static string[][] pointShadowFeatures = new string[][] {
				new string[] { "", "SHADOWS_CUBE" },
				new string[] { "", "SHADOWS_SOFT" },
				new string[] { "", "LIGHTMAP_SHADOW_MIXING" },
				new string[] { "", "SHADOWS_SHADOWMASK" },
			};

		static string[][] fwdBaseGIFeatures = new string[][] {
				new string[] { "", "LIGHTPROBE_SH" },
				new string[] { "", "LIGHTMAP_ON", "DYNAMICLIGHTMAP_ON" },
			};

		// Only relevant when LIGHTMAP_ON or DYNAMICLIGHTMAP_ON is enabled
		string[][] fwdBaseLightmapFeatures = new string[][] {
			new string[] { "", "DIRLIGHTMAP_COMBINED" },
		};
		*/

		static string[][] instancingFeatures = new string[][] {
			new string[] { "", "INSTANCING_ON" },
		};

		static string[][] directionalLightWithScreenShadowsFeatures = new string[][] {
			new string[] { "DIRECTIONAL" },
			new string[] { "SHADOWS_SCREEN" },
		};

		/*
		static string[][] pointLightWithSoftCubeShadowsFeatures = new string[][] {
			new string[] { "POINT" },
			new string[] { "SHADOWS_CUBE" },
			new string[] { "SHADOWS_SOFT" },
		};
		*/

		static string[][] pointLightWithNoShadowsFeatures = new string[][] {
			new string[] { "POINT" },
		};

		static string[][] gltfFwdMaterialFeatures = new string[][] {
			new string[] { "", "_NORMALMAP" },
			new string[] { "", "_ALPHATEST_ON", "_ALPHABLEND_ON", /*"_ALPHAPREMULTIPLY_ON"*/ },
		};

		static string[][] gltfFwdBaseMaterialFeatures = new string[][] {
			new string[] { "", "_EMISSION" },
		};

		static string[][] gltfDeferredMaterialFeatures = new string[][] {
			new string[] { "", "_NORMALMAP" },
			new string[] { "", "_ALPHATEST_ON", "_ALPHABLEND_ON", /*"_ALPHAPREMULTIPLY_ON"*/ },
			new string[] { "", "_EMISSION" },
		};

		static string[][] gltfShadowCasterFeatures = new string[][] {
			new string[] { "", "_ALPHATEST_ON" },
			new string[] { "SHADOWS_DEPTH", "SHADOWS_CUBE" }
		};

		static string[][] gltfMetallicRoughnessFeatures = new string[][] {
				new string[] { "", "_METALLICGLOSSMAP" },
		};

		static string[][] gltfSpecularGlossinessFeatures = new string[][] {
				new string[] { "", "_SPECGLOSSMAP" },
		};

		[MenuItem("Wildspace/BuildGltfShaderVariants")]
		public static void BuildGltfShaderVariants() {
			ShaderVariantCollection svc = new ShaderVariantCollection();

			Shader metallicRoughnessShader = Shader.Find("GLTF/PbrMetallicRoughness");
			Shader specularGlossinessShader = Shader.Find("GLTF/PbrSpecularGlossiness");

			// Forward*
			var gltfFwdPassFeatures = gltfFwdMaterialFeatures;
			// ForwardBase
			var gltfFwdBasePassVariants = instancingFeatures.Concat( gltfFwdPassFeatures ).Concat(gltfFwdBaseMaterialFeatures).Concat(directionalLightWithScreenShadowsFeatures).Choices();
			// ForwardAdd
			var gltfFwdAddDirectionalVariants = gltfFwdPassFeatures.Concat(directionalLightWithScreenShadowsFeatures).Choices();
			var gltfFwdAddPointVariants = gltfFwdPassFeatures.Concat(pointLightWithNoShadowsFeatures).Choices();
			var gltfFwdAddVariants = gltfFwdAddDirectionalVariants.Concat(gltfFwdAddPointVariants);
			// Deferred
			var gltfDeferredPassVariants = instancingFeatures.Concat(gltfDeferredMaterialFeatures).Choices();
			// ShadowCaster
			var gltfShadowCasterPassVariants = instancingFeatures.Concat(gltfShadowCasterFeatures).Choices();

			if (metallicRoughnessShader) {
				var metallicRoughnessVariants = gltfMetallicRoughnessFeatures.Choices();
				var metallicRoughnessFwdBaseVariants = gltfFwdBasePassVariants.SimpleJoin(metallicRoughnessVariants);
				var metallicRoughnessFwdAddVariants = gltfFwdAddVariants.SimpleJoin(metallicRoughnessVariants);
				var metallicRoughnessDeferredVariants = gltfDeferredPassVariants.SimpleJoin(metallicRoughnessVariants);

				var metallicRoughnessShaderVariants = EnumShaderPassVariants(metallicRoughnessShader, PassType.ShadowCaster, gltfShadowCasterPassVariants);
				metallicRoughnessShaderVariants = metallicRoughnessShaderVariants.Concat(EnumShaderPassVariants(metallicRoughnessShader, PassType.ForwardBase, metallicRoughnessFwdBaseVariants));
				metallicRoughnessShaderVariants = metallicRoughnessShaderVariants.Concat(EnumShaderPassVariants(metallicRoughnessShader, PassType.ForwardAdd, metallicRoughnessFwdAddVariants));
				metallicRoughnessShaderVariants = metallicRoughnessShaderVariants.Concat(EnumShaderPassVariants(metallicRoughnessShader, PassType.Deferred, metallicRoughnessDeferredVariants));

				foreach (var variant in metallicRoughnessShaderVariants ) {
					svc.Add(variant);
				}
			}

			if (specularGlossinessShader) {
				var specularGlossinessVariants = gltfSpecularGlossinessFeatures.Choices();
				var specularGlossinessFwdBaseVariants = gltfFwdBasePassVariants.SimpleJoin(specularGlossinessVariants);
				var specularGlossinessFwdAddVariants = gltfFwdAddVariants.SimpleJoin(specularGlossinessVariants);
				var specularGlossinessDeferredVariants = gltfDeferredPassVariants.SimpleJoin(specularGlossinessVariants);

				var specularGlossinessShaderVariants = EnumShaderPassVariants(specularGlossinessShader, PassType.ShadowCaster, gltfShadowCasterPassVariants);
				specularGlossinessShaderVariants = specularGlossinessShaderVariants.Concat(EnumShaderPassVariants(specularGlossinessShader, PassType.ForwardBase, specularGlossinessFwdBaseVariants));
				specularGlossinessShaderVariants = specularGlossinessShaderVariants.Concat(EnumShaderPassVariants(specularGlossinessShader, PassType.ForwardAdd, specularGlossinessFwdAddVariants));
				specularGlossinessShaderVariants = specularGlossinessShaderVariants.Concat(EnumShaderPassVariants(specularGlossinessShader, PassType.Deferred, specularGlossinessDeferredVariants));

				foreach (var variant in specularGlossinessShaderVariants) {
					svc.Add(variant);
				}
			}

			string outputPath = EditorUtility.SaveFilePanelInProject("Save variants", "RuntimeShaders", "shadervariants", "Hello");
			if (!string.IsNullOrEmpty(outputPath)) {
				AssetDatabase.CreateAsset(svc, outputPath);
				AssetDatabase.Refresh();
			}
		}
	}
}