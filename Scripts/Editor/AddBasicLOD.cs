﻿using UnityEngine;
using System.Collections;
using UnityEditor;

public class AddBasicLOD : MonoBehaviour {


	[MenuItem("Libonati/LOD/AddBasicLOD", false, 50)]
	public static void AddBasicLODToSelectedObjects(){
		//Loop through all selected transforms and their children
		foreach (Transform tP in UnityEditor.Selection.transforms) {
			addLODToTransforms(tP);
		}
	}
	//Recursive processing of transforms
	private static void addLODToTransforms(Transform tP){
		addLODToTransform (tP);
		foreach (Transform t in tP) {
			if (t.childCount > 0) {
				addLODToTransforms (t);
			} else {
				addLODToTransform (t);
			}
		}
	}
	private static void addLODToTransform(Transform t){
		MeshRenderer ren = t.gameObject.GetComponent<MeshRenderer> ();
		if (ren == null) {
			return;
		}
		LODGroup lod = ren.gameObject.AddComponent<LODGroup> ();
		LOD[] newLODS = new LOD[1];
		newLODS [0].renderers = new Renderer[1]{ ren };
		newLODS [0].screenRelativeTransitionHeight = .1f;
		lod.SetLODs (newLODS);
	}
	[MenuItem("Libonati/LOD/SetAllLODToSelectedLOD", false, 50)]
	public static void SetAllLODToSelectedLODObject(){
		LODGroup target = UnityEditor.Selection.activeGameObject.GetComponent<LODGroup> ();
		if (target == null) {
			Debug.LogWarning ("Must select gameobject with a LODGroup component");
			return;
		}
		LODGroup[] lods = GameObject.FindObjectsOfType<LODGroup> ();
		foreach (LODGroup l in lods) {
			l.animateCrossFading = target.animateCrossFading;
			l.enabled = target.enabled;
			l.fadeMode = target.fadeMode;
			LOD[] targetLODS = target.GetLODs ();
			LOD[] newLODS = l.GetLODs ();
			for(int i=0; i<newLODS.Length; i++){
				try{
					newLODS [i].screenRelativeTransitionHeight = targetLODS [i].screenRelativeTransitionHeight;
					newLODS [i].fadeTransitionWidth = targetLODS [i].fadeTransitionWidth;
				}catch{
				}
			}
			l.SetLODs (newLODS);
		}
	}
} 
